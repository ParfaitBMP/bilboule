$(function () {
    tinymce.init({
        selector: "textarea.acc-desc",
        theme: "modern",
        height: 100,
        plugins: [
            'advlist autolink lists link charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars fullscreen',
            'insertdatetime nonbreaking save table contextmenu directionality',
            'emoticons paste textcolor colorpicker textpattern'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
        toolbar2: 'print preview | forecolor backcolor emoticons',
        image_advtab: true
    });
    tinymce.suffix = ".min";
});