<?php

namespace App\Repository\Ad;

use App\Entity\AnnonceTerrain;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AnnonceTerrain|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnnonceTerrain|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnnonceTerrain[]    findAll()
 * @method AnnonceTerrain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceTerrainRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnnonceTerrain::class);
    }

    /**
     * @return AnnonceTerrain[] Returns an array of AnnonceTerrain objects
     */
    public function findByOwner(User $user)
    {
        return $this->createQueryBuilder('a')
            ->setParameter('user', $user)
            ->join('a.terrain', 't')
            ->addSelect('t')
            ->where('t.owner = :user')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?AnnonceTerrain
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
