<?php

namespace App\Repository\Ad;

use App\Entity\AnnonceChambre;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AnnonceChambre|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnnonceChambre|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnnonceChambre[]    findAll()
 * @method AnnonceChambre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceChambreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnnonceChambre::class);
    }

    /**
     * @return AnnonceAppartement[] Returns an array of AnnonceAppartement objects
     */
    public function findByOwner(User $user)
    {
        return $this->createQueryBuilder('a')
            ->setParameter('user', $user)
            ->join('a.chambre', 't')
            ->addSelect('t')
            ->where('t.owner = :user')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?AnnonceChambre
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
