<?php

namespace App\Repository\Ad;

use App\Entity\AnnonceStudio;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AnnonceStudio|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnnonceStudio|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnnonceStudio[]    findAll()
 * @method AnnonceStudio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceStudioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnnonceStudio::class);
    }

    // /**
    //  * @return AnnonceStudio[] Returns an array of AnnonceStudio objects
    //  */
    
    public function findByOwner(User $user)
    {
        return $this->createQueryBuilder('a')
            ->setParameter('user', $user)
            ->join('a.studio', 't')
            ->addSelect('t')
            ->where('t.owner = :user')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?AnnonceStudio
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
