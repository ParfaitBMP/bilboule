<?php

namespace App\Repository\Ad;

use App\Entity\AnnonceMaison;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AnnonceMaison|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnnonceMaison|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnnonceMaison[]    findAll()
 * @method AnnonceMaison[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceMaisonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnnonceMaison::class);
    }

    /**
     * @return AnnonceMaison[] Returns an array of AnnonceMaison objects
     */
    public function findByOwner(User $user)
    {
        return $this->createQueryBuilder('a')
            ->setParameter('user', $user)
            ->join('a.maison', 't')
            ->addSelect('t')
            ->where('t.owner = :user')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?AnnonceMaison
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
