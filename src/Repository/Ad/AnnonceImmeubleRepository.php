<?php

namespace App\Repository\Ad;

use App\Entity\AnnonceImmeuble;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AnnonceImmeuble|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnnonceImmeuble|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnnonceImmeuble[]    findAll()
 * @method AnnonceImmeuble[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceImmeubleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnnonceImmeuble::class);
    }

    /**
     * @return AnnonceImmeuble[] Returns an array of AnnonceImmeuble objects
     */
    public function findByOwner(User $user)
    {
        return $this->createQueryBuilder('a')
            ->setParameter('user', $user)
            ->join('a.immeuble', 't')
            ->addSelect('t')
            ->where('t.owner = :user')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?AnnonceImmeuble
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
