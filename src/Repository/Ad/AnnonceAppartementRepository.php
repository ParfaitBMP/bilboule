<?php

namespace App\Repository\Ad;

use App\Entity\AnnonceAppartement;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AnnonceAppartement|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnnonceAppartement|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnnonceAppartement[]    findAll()
 * @method AnnonceAppartement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceAppartementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnnonceAppartement::class);
    }

    /**
     * @return AnnonceAppartement[] Returns an array of AnnonceAppartement objects
     */
    public function findByOwner(User $user)
    {
        return $this->createQueryBuilder('a')
            ->setParameter('user', $user)
            ->join('a.appartement', 't')
            ->addSelect('t')
            ->where('t.owner = :user')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?AnnonceAppartement
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
