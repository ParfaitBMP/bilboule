<?php

namespace App\Repository\Ad;

use App\Entity\Annonce;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Annonce|null find($id, $lockMode = null, $lockVersion = null)
 * @method Annonce|null findOneBy(array $criteria, array $orderBy = null)
 * @method Annonce[]    findAll()
 * @method Annonce[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Annonce::class);
    }

    /**
     * @return [] Returns an array of annonce
     */
    public function findByOwner(User $user)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT * FROM annonce a
            JOIN maison th1 ON th1.id = a.maison_id
            WHERE th1.owner_id = :user_id
            ORDER BY a.id DESC
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['user_id' => $user->getId()]);
        // $stmt->execute();

        return $stmt->fetchAll();
    }

    /*
    public function findOneBySomeField($value): ?Annonce
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
