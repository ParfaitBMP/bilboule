<?php

namespace App\Repository\PJ;

use App\Entity\PjAppartement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PjAppartement|null find($id, $lockMode = null, $lockVersion = null)
 * @method PjAppartement|null findOneBy(array $criteria, array $orderBy = null)
 * @method PjAppartement[]    findAll()
 * @method PjAppartement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PjAppartementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PjAppartement::class);
    }

    // /**
    //  * @return PjAppartement[] Returns an array of PjAppartement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PjAppartement
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
