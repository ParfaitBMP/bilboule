<?php

namespace App\Repository\PJ;

use App\Entity\PjImmeuble;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PjImmeuble|null find($id, $lockMode = null, $lockVersion = null)
 * @method PjImmeuble|null findOneBy(array $criteria, array $orderBy = null)
 * @method PjImmeuble[]    findAll()
 * @method PjImmeuble[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PjImmeubleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PjImmeuble::class);
    }

    // /**
    //  * @return PjImmeuble[] Returns an array of PjImmeuble objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PjImmeuble
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
