<?php

namespace App\Repository\PJ;

use App\Entity\PjTerrain;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PjTerrain|null find($id, $lockMode = null, $lockVersion = null)
 * @method PjTerrain|null findOneBy(array $criteria, array $orderBy = null)
 * @method PjTerrain[]    findAll()
 * @method PjTerrain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PjTerrainRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PjTerrain::class);
    }

    // /**
    //  * @return PjTerrain[] Returns an array of PjTerrain objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PjTerrain
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
