<?php

namespace App\Repository\PJ;

use App\Entity\PjStudio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PjStudio|null find($id, $lockMode = null, $lockVersion = null)
 * @method PjStudio|null findOneBy(array $criteria, array $orderBy = null)
 * @method PjStudio[]    findAll()
 * @method PjStudio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PjStudioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PjStudio::class);
    }

    // /**
    //  * @return PjStudio[] Returns an array of PjStudio objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PjStudio
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
