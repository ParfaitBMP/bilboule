<?php

namespace App\Repository\PJ;

use App\Entity\PjMaison;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PjMaison|null find($id, $lockMode = null, $lockVersion = null)
 * @method PjMaison|null findOneBy(array $criteria, array $orderBy = null)
 * @method PjMaison[]    findAll()
 * @method PjMaison[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PjMaisonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PjMaison::class);
    }

    // /**
    //  * @return PjMaison[] Returns an array of PjMaison objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PjMaison
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
