<?php

namespace App\Repository\Thing;

use App\Entity\SomeThing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SomeThing|null find($id, $lockMode = null, $lockVersion = null)
 * @method SomeThing|null findOneBy(array $criteria, array $orderBy = null)
 * @method SomeThing[]    findAll()
 * @method SomeThing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SomeThingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SomeThing::class);
    }

    // /**
    //  * @return SomeThing[] Returns an array of SomeThing objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SomeThing
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
