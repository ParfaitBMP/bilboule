<?php

namespace App\Repository\Extra;

use App\Entity\ExtraTerrain;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExtraTerrain|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExtraTerrain|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExtraTerrain[]    findAll()
 * @method ExtraTerrain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExtraTerrainRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExtraTerrain::class);
    }

    // /**
    //  * @return ExtraTerrain[] Returns an array of ExtraTerrain objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExtraTerrain
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
