<?php

namespace App\Repository\Extra;

use App\Entity\ExtraLogement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExtraLogement|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExtraLogement|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExtraLogement[]    findAll()
 * @method ExtraLogement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExtraLogementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExtraLogement::class);
    }

    // /**
    //  * @return ExtraLogement[] Returns an array of ExtraLogement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExtraLogement
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
