<?php

namespace App\Repository\Extra;

use App\Entity\ExtraImmeuble;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExtraImmeuble|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExtraImmeuble|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExtraImmeuble[]    findAll()
 * @method ExtraImmeuble[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExtraImmeubleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExtraImmeuble::class);
    }

    // /**
    //  * @return ExtraImmeuble[] Returns an array of ExtraImmeuble objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExtraImmeuble
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
