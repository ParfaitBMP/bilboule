<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\Map\PlaceRepository;

class PlaceController extends AbstractController
{

    private $placeRepository;

    public function __construct(PlaceRepository $placeRepository){
        $this->placeRepository = $placeRepository;
    }
    /**
     * @Route("/place", name="place")
     */
    public function index()
    {
        return $this->render('place/index.html.twig', [
            'controller_name' => 'PlaceController',
        ]);
    }

    /**
     * @Route("/find/place/{q}", name="as_fbn", methods={"GET"}, options = { "expose" = true })
     * @param String $q
     * @return JSON
    */
    public function select2FindMyRequestByCode(Request $request, $q=null)
    {
        $q =  htmlspecialchars(strip_tags($request->query->get('q', '')));
        $places = $this->placeRepository->ajaxSelectFindByName($q);
        $list = [];
        
        foreach ($places as $place) {
            $list[] = ["key" => $place->getId(), "text" => $place->getName(), "help" => $place->getTree()];
        }

        $response = new JsonResponse();
        $response->setData($list);

        return $response;
    }
}
