<?php

namespace App\Controller\Account;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Annonce;
use App\Entity\Particulier;
use App\Entity\Entreprise;
use App\Entity\Place;
use App\Entity\Category;
use App\Entity\Appartement;
use App\Entity\Maison;
use App\Entity\Studio;
use App\Entity\Chambre;
use App\Entity\Terrain;
use App\Entity\Immeuble;
use App\Repository\Ad\AnnonceRepository;
use App\Repository\Thing\SomeThingRepository;
use App\Form\Account\ParticulierType;
use App\Form\Account\EntrepriseType;
use App\Form\ResetPasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/account")
 */
class AccountController extends AbstractController
{
    private $em;
    private $passwordEncoder;
    private $authenticationUtils;

    function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder, AuthenticationUtils $authenticationUtils)
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
        $this->authenticationUtils = $authenticationUtils;
    }

    /**
     * @Route("/", name="app_account")
     */
    public function index()
    {
        return $this->render('account/pages/dasboard.html.twig');
    }

    /**
     * @Route("/profile", name="acc_profile")
     */
    public function profileAction(Request $request, AnnonceRepository $annonceRepository)
    {
        $user = $this->getUser();
        $place = $user->getPlace()? $user->getPlace() : new Place();
        $resetPwdForm = $this->createForm(ResetPasswordType::class, $user);

        if ($user->getType() == 'PARTICULIER'){
            $userForm = $this->createForm(ParticulierType::class, $user, ['place' => $place]);
        }
        elseif($user->getType() == 'ENTREPRISE'){
            $userForm = $this->createForm(EntrepriseType::class, $user, ['place' => $place]);
        }

        //Cas de la MAJ du profile
        $userForm->handleRequest($request);

        if($userForm->isSubmitted() && $userForm->isValid())
        {
            $this->em->persist($user);
            $this->em->flush();
            $this->addFlash('acc_success_msg', 'Vos modifications ont été enregistrées avec succès');
        }

        //Cas du reset password
        $resetPwdForm->handleRequest($request);
        $error = $this->authenticationUtils->getLastAuthenticationError();

        if ($resetPwdForm->isSubmitted() && $resetPwdForm->isValid()) {
            $newPassword = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());

            if(!$this->passwordEncoder->isPasswordValid($user, $user->getOldPassword())){
                $this->addFlash('acc_error_msg', 'Mot de passe actuel erroné.');

                return $this->redirectToRoute('acc_profile');
            }

            $user->setPassword($newPassword);

            $this->em->persist($user);
            $this->em->flush();

            $this->addFlash('acc_success_msg', 'Votre mot de passe a été réinitialisé avec succès.');
        }

        return $this->render('account/pages/profile.html.twig', [
            'user' => $user,
            'userForm' => $userForm->createView(),
            'resetPwdForm' => $resetPwdForm->createView(),
            'select2params' => '{{{q}}}',
            'error' => $error,
            'annonces' => $annonceRepository->findByOwner($this->getUser()),
        ]);
    }

    /**
     * @Route("/things", name="my_things")
     */
    public function myThingsAction(SomeThingRepository $someThingRepository)
    {
           
        $things = $someThingRepository->findByOwner($this->getUser());
        $categories = $this->getDoctrine()->getRepository(Category::class)->findByActive(true);

        return $this->render('account/pages/thing/things/index.html.twig', [
            'things' => $things,
            'categories' => $categories
        ]);
    }
}
