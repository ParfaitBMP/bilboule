<?php

namespace App\Controller\Account\Ad;

use App\Entity\Annonce;
use App\Entity\AnnonceAppartement;
use App\Entity\AnnonceMaison;
use App\Entity\AnnonceStudio;
use App\Entity\AnnonceChambre;
use App\Entity\AnnonceTerrain;
use App\Entity\AnnonceImmeuble;
use App\Form\AnnonceType;
use App\Repository\Ad\AnnonceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\SiteConfig;

/**
 * @Route("/account/annonces")
 */
class AnnonceController extends AbstractController
{
    /**
     * @Route("/", name="annonce_index", methods={"GET"})
     */
    public function index(AnnonceRepository $annonceRepository): Response
    {
        $annonces = [];
        $annonces = array_merge(
            $annonces, 
            $this->getDoctrine()->getRepository(AnnonceMaison::class)->findByOwner($this->getUser())
        );
        $annonces = array_merge(
            $annonces, 
            $this->getDoctrine()->getRepository(AnnonceAppartement::class)->findByOwner($this->getUser())
        );
        $annonces = array_merge(
            $annonces, 
            $this->getDoctrine()->getRepository(AnnonceStudio::class)->findByOwner($this->getUser())
        );
        $annonces = array_merge(
            $annonces, 
            $this->getDoctrine()->getRepository(AnnonceChambre::class)->findByOwner($this->getUser())
        );
        $annonces = array_merge(
            $annonces, 
            $this->getDoctrine()->getRepository(AnnonceTerrain::class)->findByOwner($this->getUser())
        );
        $annonces = array_merge(
            $annonces, 
            $this->getDoctrine()->getRepository(AnnonceImmeuble::class)->findByOwner($this->getUser())
        );

        return $this->render('account/pages/ad/ads/index.html.twig', [
            'annonces' => $annonces,
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_show", methods={"GET"})
     */
    public function show(Annonce $annonce): Response
    {
        return $this->render('account/pages/ad/ads/show.html.twig', [
            'annonce' => $annonce,
            'ACTION_PUBLISH' => true,
        ]);
    }

    /**
     * @Route("/publish/{id}", name="annonce_publish", methods={"GET"})
     */
    public function publish(Annonce $annonce): Response
    {
        $annonce->setToPending();
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('acc_success_msg', SiteConfig::PUBLISH_MSG);

        return $this->redirectToRoute('annonce_index');
    }

    /**
     * @Route("/unpublish/{id}", name="annonce_unpublish", methods={"GET"})
     */
    public function unPublish(Annonce $annonce): Response
    {
        $annonce->setToDraft();
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

        return $this->redirectToRoute('annonce_index');
    }

    /**
     * @Route("/{id}", name="annonce_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Annonce $annonce): Response
    {
        if ($this->isCsrfTokenValid('delete'.$annonce->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($annonce);
            $entityManager->flush();
        }

        return $this->redirectToRoute('annonce_index');
    }
}
