<?php

namespace App\Controller\Account\Ad;

use App\Entity\Chambre;
use App\Entity\Category;
use App\Entity\AnnonceChambre;
use App\Form\Ad\AnnonceChambreType;
use App\Repository\Ad\AnnonceChambreRepository;
use App\Repository\CategoryRepository;
use App\Repository\FormuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Utils\SiteConfig;

/**
 * @Route("/account/annonce/chambre")
 */
class AnnonceChambreController extends AbstractController
{
    private $slugger;
    private $formuleRepository;
    private $categoryRepository;

    function __construct(SluggerInterface $slugger, FormuleRepository $formuleRepository, CategoryRepository $categoryRepository)
    {
        $this->slugger = $slugger;
        $this->formuleRepository = $formuleRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/", name="annonce_chambre_index", methods={"GET"})
     */
    public function index(AnnonceChambreRepository $annonceChambreRepository): Response
    {
        return $this->render('account/pages/ad/annonce_chambre/index.html.twig', [
            'annonce_chambres' => $annonceChambreRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{id}", name="annonce_chambre_new", methods={"GET","POST"})
     */
    public function new(Request $request, Chambre $chambre): Response
    {
        $annonceChambre = new AnnonceChambre();
        $annonceChambre->setChambre($chambre);
        $category = $this->categoryRepository->findOneByCode(Category::CODE_ROOM);
        $annonceChambre->setCategory($category);
        $form = $this->createForm(AnnonceChambreType::class, $annonceChambre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonceChambre->setSlug($this->slugger->slug($annonceChambre->getName()));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($annonceChambre);
            $entityManager->flush();

            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('annonce_index');
        }

        return $this->render('account/pages/ad/annonce_chambre/new.html.twig', [
            'annonce' => $annonceChambre,
            'thing' => $chambre,
            'formules' =>  $this->formuleRepository->findAll(),
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_chambre_show", methods={"GET"})
     */
    public function show(AnnonceChambre $annonceChambre): Response
    {
        return $this->render('account/pages/ad/annonce_chambre/show.html.twig', [
            'annonce' => $annonceChambre,
            'thing' => $annonceChambre->getChambre(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="annonce_chambre_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AnnonceChambre $annonceChambre): Response
    {
        $form = $this->createForm(AnnonceChambreType::class, $annonceChambre, ['TYPE_FORM' => 'EDIT']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonceChambre->setSlug($this->slugger->slug($annonceChambre->getName()));

            //On remet en attente en cas de modification
            if ($annonceChambre->getState() == AnnonceChambre::STATE_DONE) {
                $annonceChambre->setToPending();
                $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);
                $this->addFlash('acc_success_msg', SiteConfig::PUBLISH_MSG);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('annonce_index');
        }

        return $this->render('account/pages/ad/annonce_chambre/edit.html.twig', [
            'annonce' => $annonceChambre,
            'thing' => $annonceChambre->getChambre(),
            'formules' =>  $this->formuleRepository->findAll(),
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_chambre_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AnnonceChambre $annonceChambre): Response
    {
        if ($this->isCsrfTokenValid('delete'.$annonceChambre->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($annonceChambre);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::DELETE_MSG);
        }

        return $this->redirectToRoute('annonce_index');
    }
}