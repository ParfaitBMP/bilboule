<?php

namespace App\Controller\Account\Ad;

use App\Entity\Appartement;
use App\Entity\Category;
use App\Entity\AnnonceAppartement;
use App\Form\Ad\AnnonceAppartementType;
use App\Repository\Ad\AnnonceAppartementRepository;
use App\Repository\CategoryRepository;
use App\Repository\FormuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Utils\SiteConfig;

/**
 * @Route("/account/annonce/appartement")
 */
class AnnonceAppartementController extends AbstractController
{
    private $slugger;
    private $formuleRepository;
    private $categoryRepository;

    function __construct(SluggerInterface $slugger, FormuleRepository $formuleRepository, CategoryRepository $categoryRepository)
    {
        $this->slugger = $slugger;
        $this->formuleRepository = $formuleRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/", name="annonce_appartement_index", methods={"GET"})
     */
    public function index(AnnonceAppartementRepository $annonceAppartementRepository): Response
    {
        return $this->render('account/pages/ad/annonce_appartement/index.html.twig', [
            'annonce_appartements' => $annonceAppartementRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{id}", name="annonce_appartement_new", methods={"GET","POST"})
     */
    public function new(Request $request, Appartement $appartement): Response
    {
        $annonceAppartement = new AnnonceAppartement();
        $annonceAppartement->setAppartement($appartement);
        $category = $this->categoryRepository->findOneByCode(Category::CODE_APPART);
        $annonceAppartement->setCategory($category);
        $form = $this->createForm(AnnonceAppartementType::class, $annonceAppartement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonceAppartement->setSlug($this->slugger->slug($annonceAppartement->getName()));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($annonceAppartement);
            $entityManager->flush();

            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('annonce_index');
        }

        return $this->render('account/pages/ad/annonce_appartement/new.html.twig', [
            'annonce' => $annonceAppartement,
            'thing' => $appartement,
            'formules' =>  $this->formuleRepository->findAll(),
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_appartement_show", methods={"GET"})
     */
    public function show(AnnonceAppartement $annonceAppartement): Response
    {
        return $this->render('account/pages/ad/annonce_appartement/show.html.twig', [
            'annonce' => $annonceAppartement,
            'thing' => $annonceAppartement->getAppartement(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="annonce_appartement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AnnonceAppartement $annonceAppartement): Response
    {
        $form = $this->createForm(AnnonceAppartementType::class, $annonceAppartement, ['TYPE_FORM' => 'EDIT']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonceAppartement->setSlug($this->slugger->slug($annonceAppartement->getName()));

            //On remet en attente en cas de modification
            if ($annonceAppartement->getState() == AnnonceAppartement::STATE_DONE) {
                $annonceAppartement->setToPending();
                $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);
                $this->addFlash('acc_success_msg', SiteConfig::PUBLISH_MSG);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('annonce_index');
        }

        return $this->render('account/pages/ad/annonce_appartement/edit.html.twig', [
            'annonce' => $annonceAppartement,
            'thing' => $annonceAppartement->getAppartement(),
            'formules' =>  $this->formuleRepository->findAll(),
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_appartement_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AnnonceAppartement $annonceAppartement): Response
    {
        if ($this->isCsrfTokenValid('delete'.$annonceAppartement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($annonceAppartement);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::DELETE_MSG);
        }

        return $this->redirectToRoute('annonce_index');
    }
}
