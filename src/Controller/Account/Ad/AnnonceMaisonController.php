<?php

namespace App\Controller\Account\Ad;

use App\Entity\Maison;
use App\Entity\Category;
use App\Entity\AnnonceMaison;
use App\Form\Ad\AnnonceMaisonType;
use App\Repository\Ad\AnnonceMaisonRepository;
use App\Repository\CategoryRepository;
use App\Repository\FormuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Utils\SiteConfig;

/**
 * @Route("/account/annonce/maison")
 */
class AnnonceMaisonController extends AbstractController
{
    private $slugger;
    private $formuleRepository;
    private $categoryRepository;

    function __construct(SluggerInterface $slugger, FormuleRepository $formuleRepository, CategoryRepository $categoryRepository)
    {
        $this->slugger = $slugger;
        $this->formuleRepository = $formuleRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/", name="annonce_maison_index", methods={"GET"})
     */
    public function index(AnnonceMaisonRepository $annonceMaisonRepository): Response
    {
        return $this->render('account/pages/ad/annonce_maison/index.html.twig', [
            'annonce_maisons' => $annonceMaisonRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{id}", name="annonce_maison_new", methods={"GET","POST"})
     */
    public function new(Request $request, Maison $maison): Response
    {
        $annonceMaison = new AnnonceMaison();
        $annonceMaison->setMaison($maison);
        $category = $this->categoryRepository->findOneByCode(Category::CODE_HOME);
        $annonceMaison->setCategory($category);
        $form = $this->createForm(AnnonceMaisonType::class, $annonceMaison);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonceMaison->setSlug($this->slugger->slug($annonceMaison->getName()));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($annonceMaison);
            $entityManager->flush();

            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('annonce_index');
        }

        return $this->render('account/pages/ad/annonce_maison/new.html.twig', [
            'annonce' => $annonceMaison,
            'thing' => $maison,
            'formules' =>  $this->formuleRepository->findAll(),
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_maison_show", methods={"GET"})
     */
    public function show(AnnonceMaison $annonceMaison): Response
    {
        return $this->render('account/pages/ad/annonce_maison/show.html.twig', [
            'annonce' => $annonceMaison,
            'thing' => $annonceMaison->getMaison(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="annonce_maison_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AnnonceMaison $annonceMaison): Response
    {
        $form = $this->createForm(AnnonceMaisonType::class, $annonceMaison, ['TYPE_FORM' => 'EDIT']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonceMaison->setSlug($this->slugger->slug($annonceMaison->getName()));

            //On remet en attente en cas de modification
            if ($annonceMaison->getState() == AnnonceMaison::STATE_DONE) {
                $annonceMaison->setToPending();
                $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);
                $this->addFlash('acc_success_msg', SiteConfig::PUBLISH_MSG);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('annonce_index');
        }

        return $this->render('account/pages/ad/annonce_maison/edit.html.twig', [
            'annonce' => $annonceMaison,
            'thing' => $annonceMaison->getMaison(),
            'formules' =>  $this->formuleRepository->findAll(),
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_maison_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AnnonceMaison $annonceMaison): Response
    {
        if ($this->isCsrfTokenValid('delete'.$annonceMaison->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($annonceMaison);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::DELETE_MSG);
        }

        return $this->redirectToRoute('annonce_index');
    }
}
