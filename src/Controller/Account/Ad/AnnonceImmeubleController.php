<?php

namespace App\Controller\Account\Ad;

use App\Entity\Immeuble;
use App\Entity\Category;
use App\Entity\AnnonceImmeuble;
use App\Form\Ad\AnnonceImmeubleType;
use App\Repository\Ad\AnnonceImmeubleRepository;
use App\Repository\CategoryRepository;
use App\Repository\FormuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Utils\SiteConfig;

/**
 * @Route("/account/annonce/immeuble")
 */
class AnnonceImmeubleController extends AbstractController
{
    private $slugger;
    private $formuleRepository;
    private $categoryRepository;

    function __construct(SluggerInterface $slugger, FormuleRepository $formuleRepository, CategoryRepository $categoryRepository)
    {
        $this->slugger = $slugger;
        $this->formuleRepository = $formuleRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/", name="annonce_immeuble_index", methods={"GET"})
     */
    public function index(AnnonceImmeubleRepository $annonceImmeubleRepository): Response
    {
        return $this->render('account/pages/ad/annonce_immeuble/index.html.twig', [
            'annonce_immeubles' => $annonceImmeubleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{id}", name="annonce_immeuble_new", methods={"GET","POST"})
     */
    public function new(Request $request, Immeuble $immeuble): Response
    {
        $annonceImmeuble = new AnnonceImmeuble();
        $annonceImmeuble->setImmeuble($immeuble);
        $category = $this->categoryRepository->findOneByCode(Category::CODE_BUILDING);
        $annonceImmeuble->setCategory($category);
        $form = $this->createForm(AnnonceImmeubleType::class, $annonceImmeuble);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonceImmeuble->setSlug($this->slugger->slug($annonceImmeuble->getName()));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($annonceImmeuble);
            $entityManager->flush();

            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('annonce_index');
        }

        return $this->render('account/pages/ad/annonce_immeuble/new.html.twig', [
            'annonce' => $annonceImmeuble,
            'thing' => $immeuble,
            'formules' =>  $this->formuleRepository->findAll(),
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_immeuble_show", methods={"GET"})
     */
    public function show(AnnonceImmeuble $annonceImmeuble): Response
    {
        return $this->render('account/pages/ad/annonce_immeuble/show.html.twig', [
            'annonce' => $annonceImmeuble,
            'thing' => $annonceImmeuble->getImmeuble(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="annonce_immeuble_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AnnonceImmeuble $annonceImmeuble): Response
    {
        $form = $this->createForm(AnnonceImmeubleType::class, $annonceImmeuble, ['TYPE_FORM' => 'EDIT']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonceImmeuble->setSlug($this->slugger->slug($annonceImmeuble->getName()));

            //On remet en attente en cas de modification
            if ($annonceImmeuble->getState() == AnnonceImmeuble::STATE_DONE) {
                $annonceImmeuble->setToPending();
                $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);
                $this->addFlash('acc_success_msg', SiteConfig::PUBLISH_MSG);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('annonce_index');
        }

        return $this->render('account/pages/ad/annonce_immeuble/edit.html.twig', [
            'annonce' => $annonceImmeuble,
            'thing' => $annonceImmeuble->getImmeuble(),
            'formules' =>  $this->formuleRepository->findAll(),
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_immeuble_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AnnonceImmeuble $annonceImmeuble): Response
    {
        if ($this->isCsrfTokenValid('delete'.$annonceImmeuble->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($annonceImmeuble);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::DELETE_MSG);
        }

        return $this->redirectToRoute('annonce_index');
    }
}
