<?php

namespace App\Controller\Account\Ad;

use App\Entity\Studio;
use App\Entity\Category;
use App\Entity\AnnonceStudio;
use App\Form\Ad\AnnonceStudioType;
use App\Repository\Ad\AnnonceStudioRepository;
use App\Repository\CategoryRepository;
use App\Repository\FormuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Utils\SiteConfig;

/**
 * @Route("/account/annonce/studio")
 */
class AnnonceStudioController extends AbstractController
{
    private $slugger;
    private $formuleRepository;
    private $categoryRepository;

    function __construct(SluggerInterface $slugger, FormuleRepository $formuleRepository, CategoryRepository $categoryRepository)
    {
        $this->slugger = $slugger;
        $this->formuleRepository = $formuleRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/", name="annonce_studio_index", methods={"GET"})
     */
    public function index(AnnonceStudioRepository $annonceStudioRepository): Response
    {
        return $this->render('account/pages/ad/annonce_studio/index.html.twig', [
            'annonce_studios' => $annonceStudioRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{id}", name="annonce_studio_new", methods={"GET","POST"})
     */
    public function new(Request $request, Studio $studio): Response
    {
        $annonceStudio = new AnnonceStudio();
        $annonceStudio->setStudio($studio);
        $category = $this->categoryRepository->findOneByCode(Category::CODE_STUDIO);
        $annonceStudio->setCategory($category);
        $form = $this->createForm(AnnonceStudioType::class, $annonceStudio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonceStudio->setSlug($this->slugger->slug($annonceStudio->getName()));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($annonceStudio);
            $entityManager->flush();

            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('annonce_index');
        }

        return $this->render('account/pages/ad/annonce_studio/new.html.twig', [
            'annonce' => $annonceStudio,
            'thing' => $studio,
            'formules' =>  $this->formuleRepository->findAll(),
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_studio_show", methods={"GET"})
     */
    public function show(AnnonceStudio $annonceStudio): Response
    {
        return $this->render('account/pages/ad/annonce_studio/show.html.twig', [
            'annonce' => $annonceStudio,
            'thing' => $annonceStudio->getStudio(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="annonce_studio_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AnnonceStudio $annonceStudio): Response
    {
        $form = $this->createForm(AnnonceStudioType::class, $annonceStudio, ['TYPE_FORM' => 'EDIT']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonceStudio->setSlug($this->slugger->slug($annonceStudio->getName()));

            //On remet en attente en cas de modification
            if ($annonceStudio->getState() == AnnonceStudio::STATE_DONE) {
                $annonceStudio->setToPending();
                $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);
                $this->addFlash('acc_success_msg', SiteConfig::PUBLISH_MSG);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('annonce_index');
        }

        return $this->render('account/pages/ad/annonce_studio/edit.html.twig', [
            'annonce' => $annonceStudio,
            'thing' => $annonceStudio->getStudio(),
            'formules' =>  $this->formuleRepository->findAll(),
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_studio_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AnnonceStudio $annonceStudio): Response
    {
        if ($this->isCsrfTokenValid('delete'.$annonceStudio->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($annonceStudio);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::DELETE_MSG);
        }

        return $this->redirectToRoute('annonce_index');
    }
}
