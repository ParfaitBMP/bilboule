<?php

namespace App\Controller\Account\Ad;

use App\Entity\Terrain;
use App\Entity\Category;
use App\Entity\AnnonceTerrain;
use App\Form\Ad\AnnonceTerrainType;
use App\Repository\Ad\AnnonceTerrainRepository;
use App\Repository\CategoryRepository;
use App\Repository\FormuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Utils\SiteConfig;

/**
 * @Route("/account/annonce/terrain")
 */
class AnnonceTerrainController extends AbstractController
{
    private $slugger;
    private $formuleRepository;
    private $categoryRepository;

    function __construct(SluggerInterface $slugger, FormuleRepository $formuleRepository, CategoryRepository $categoryRepository)
    {
        $this->slugger = $slugger;
        $this->formuleRepository = $formuleRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/", name="annonce_terrain_index", methods={"GET"})
     */
    public function index(AnnonceTerrainRepository $annonceTerrainRepository): Response
    {
        return $this->render('account/pages/ad/annonce_terrain/index.html.twig', [
            'annonce_terrains' => $annonceTerrainRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{id}", name="annonce_terrain_new", methods={"GET","POST"})
     */
    public function new(Request $request, Terrain $terrain): Response
    {
        $annonceTerrain = new AnnonceTerrain();
        $annonceTerrain->setTerrain($terrain);
        $category = $this->categoryRepository->findOneByCode(Category::CODE_TERRAIN);
        $annonceTerrain->setCategory($category);
        $form = $this->createForm(AnnonceTerrainType::class, $annonceTerrain);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonceTerrain->setSlug($this->slugger->slug($annonceTerrain->getName()));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($annonceTerrain);
            $entityManager->flush();

            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('annonce_index');
        }

        return $this->render('account/pages/ad/annonce_terrain/new.html.twig', [
            'annonce' => $annonceTerrain,
            'thing' => $terrain,
            'formules' =>  $this->formuleRepository->findAll(),
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_terrain_show", methods={"GET"})
     */
    public function show(AnnonceTerrain $annonceTerrain): Response
    {
        return $this->render('account/pages/ad/annonce_terrain/show.html.twig', [
            'annonce' => $annonceTerrain,
            'thing' => $annonceTerrain->getTerrain(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="annonce_terrain_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AnnonceTerrain $annonceTerrain): Response
    {
        $form = $this->createForm(AnnonceTerrainType::class, $annonceTerrain, ['TYPE_FORM' => 'EDIT']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonceTerrain->setSlug($this->slugger->slug($annonceTerrain->getName()));

            //On remet en attente en cas de modification
            if ($annonceTerrain->getState() == AnnonceTerrain::STATE_DONE) {
                $annonceTerrain->setToPending();
                $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);
                $this->addFlash('acc_success_msg', SiteConfig::PUBLISH_MSG);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('annonce_index');
        }

        return $this->render('account/pages/ad/annonce_terrain/edit.html.twig', [
            'annonce' => $annonceTerrain,
            'thing' => $annonceTerrain->getTerrain(),
            'formules' =>  $this->formuleRepository->findAll(),
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="annonce_terrain_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AnnonceTerrain $annonceTerrain): Response
    {
        if ($this->isCsrfTokenValid('delete'.$annonceTerrain->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($annonceTerrain);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::DELETE_MSG);
        }

        return $this->redirectToRoute('annonce_index');
    }
}