<?php

namespace App\Controller\Account\Thing;

use App\Entity\Immeuble;
use App\Form\Thing\ImmeubleType;
use App\Repository\Thing\ImmeubleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\SiteConfig;

/**
 * @Route("/account/immeuble")
 */
class ImmeubleController extends AbstractController
{
    /**
     * @Route("/", name="immeuble_index", methods={"GET"})
     */
    public function index(ImmeubleRepository $immeubleRepository): Response
    {
        return $this->render('account/pages/thing/immeuble/index.html.twig', [
            'immeubles' => $immeubleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="immeuble_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $immeuble = new Immeuble();
        $immeuble->setOwner($this->getUser());
        $form = $this->createForm(ImmeubleType::class, $immeuble);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            //vérification si les dimensions ont été définies
            if(!$request->request->get('hasDimension'))
                $immeuble->setDimension(null);


            $entityManager->persist($immeuble);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('my_things');
        }

        return $this->render('account/pages/thing/immeuble/new.html.twig', [
            'thing' => $immeuble,
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="immeuble_show", methods={"GET"})
     */
    public function show(Immeuble $immeuble): Response
    {
        return $this->render('account/pages/thing/immeuble/show.html.twig', [
            'thing' => $immeuble,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="immeuble_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Immeuble $immeuble): Response
    {
        $form = $this->createForm(ImmeubleType::class, $immeuble);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            
            //vérification si les dimensions ont été défini
            if(!$request->request->get('hasDimension')){
                if ($immeuble->getDimension()) {
                    $entityManager->remove($immeuble->getDimension());
                    $immeuble->setDimension(null);
                }
            }else{
                if($immeuble->getDimension() && !$immeuble->getDimension()->hasValues()) {
                    $immeuble->setDimension((null));
                }
            }
            
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('my_things');
        }

        return $this->render('account/pages/thing/immeuble/edit.html.twig', [
            'thing' => $immeuble,
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}/delete", name="immeuble_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Immeuble $immeuble): Response
    {
        if ($this->isCsrfTokenValid('delete'.$immeuble->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($immeuble);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::DELETE_MSG);
        }

        return $this->redirectToRoute('my_things');
    }
}
