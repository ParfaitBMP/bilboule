<?php

namespace App\Controller\Account\Thing;

use App\Entity\Studio;
use App\Form\Thing\StudioType;
use App\Repository\Thing\StudioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\SiteConfig;

/**
 * @Route("/account/studio")
 */
class StudioController extends AbstractController
{
    /**
     * @Route("/", name="studio_index", methods={"GET"})
     */
    public function index(StudioRepository $studioRepository): Response
    {
        return $this->render('account/pages/thing/studio/index.html.twig', [
            'studios' => $studioRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="studio_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $studio = new Studio();
        $studio->setOwner($this->getUser());
        $form = $this->createForm(StudioType::class, $studio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            //vérification si les dimensions ont été définies
            if(!$request->request->get('hasDimension'))
                $studio->setDimension(null);


            $entityManager->persist($studio);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('my_things');
        }

        return $this->render('account/pages/thing/studio/new.html.twig', [
            'thing' => $studio,
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="studio_show", methods={"GET"})
     */
    public function show(Studio $studio): Response
    {
        return $this->render('account/pages/thing/studio/show.html.twig', [
            'thing' => $studio,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="studio_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Studio $studio): Response
    {
        $form = $this->createForm(StudioType::class, $studio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            
            //vérification si les dimensions ont été défini
            if(!$request->request->get('hasDimension')){
                if ($studio->getDimension()) {
                    $entityManager->remove($studio->getDimension());
                    $studio->setDimension(null);
                }
            }else{
                if($studio->getDimension() && !$studio->getDimension()->hasValues()) {
                    $studio->setDimension((null));
                }
            }
            
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('my_things');
        }

        return $this->render('account/pages/thing/studio/edit.html.twig', [
            'thing' => $studio,
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}/delete", name="studio_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Studio $studio): Response
    {
        if ($this->isCsrfTokenValid('delete'.$studio->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($studio);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::DELETE_MSG);
        }

        return $this->redirectToRoute('my_things');
    }
}
