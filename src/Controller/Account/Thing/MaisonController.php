<?php

namespace App\Controller\Account\Thing;

use App\Entity\Maison;
use App\Form\Thing\MaisonType;
use App\Repository\Thing\MaisonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\SiteConfig;

/**
 * @Route("/account/house")
 */
class MaisonController extends AbstractController
{
    /**
     * @Route("/", name="maison_index", methods={"GET"})
     */
    public function index(MaisonRepository $maisonRepository): Response
    {
        return $this->render('account/pages/thing/maison/index.html.twig', [
            'maisons' => $maisonRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="maison_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $maison = new Maison();
        $maison->setOwner($this->getUser());
        $form = $this->createForm(MaisonType::class, $maison);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            //vérification si les dimensions ont été définies
            if(!$request->request->get('hasDimension'))
                $maison->setDimension(null);


            $entityManager->persist($maison);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('my_things');
        }

        return $this->render('account/pages/thing/maison/new.html.twig', [
            'thing' => $maison,
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="maison_show", methods={"GET"})
     */
    public function show(Maison $maison): Response
    {
        return $this->render('account/pages/thing/maison/show.html.twig', [
            'thing' => $maison,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="maison_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Maison $maison): Response
    {
        $form = $this->createForm(MaisonType::class, $maison);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            
            //vérification si les dimensions ont été défini
            if(!$request->request->get('hasDimension')){
                if ($maison->getDimension()) {
                    $entityManager->remove($maison->getDimension());
                    $maison->setDimension(null);
                }
            }else{
                if($maison->getDimension() && !$maison->getDimension()->hasValues()) {
                    $maison->setDimension((null));
                }
            }
            
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('my_things');
        }

        return $this->render('account/pages/thing/maison/edit.html.twig', [
            'thing' => $maison,
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}/delete", name="maison_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Maison $maison): Response
    {
        if ($this->isCsrfTokenValid('delete'.$maison->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($maison);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::DELETE_MSG);
        }

        return $this->redirectToRoute('my_things');
    }
}
