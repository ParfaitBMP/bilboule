<?php

namespace App\Controller\Account\Thing;

use App\Entity\Chambre;
use App\Form\Thing\ChambreType;
use App\Repository\Thing\ChambreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\SiteConfig;

/**
 * @Route("/account/chambre")
 */
class ChambreController extends AbstractController
{
    /**
     * @Route("/", name="chambre_index", methods={"GET"})
     */
    public function index(ChambreRepository $chambreRepository): Response
    {
        return $this->render('account/pages/thing/chambre/index.html.twig', [
            'chambres' => $chambreRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="chambre_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $chambre = new Chambre();
        $chambre->setOwner($this->getUser());
        $form = $this->createForm(ChambreType::class, $chambre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            //vérification si les dimensions ont été définies
            if(!$request->request->get('hasDimension'))
                $chambre->setDimension(null);


            $entityManager->persist($chambre);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('my_things');
        }

        return $this->render('account/pages/thing/chambre/new.html.twig', [
            'thing' => $chambre,
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="chambre_show", methods={"GET"})
     */
    public function show(Chambre $chambre): Response
    {
        return $this->render('account/pages/thing/chambre/show.html.twig', [
            'thing' => $chambre,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="chambre_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Chambre $chambre): Response
    {
        $form = $this->createForm(ChambreType::class, $chambre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            
            //vérification si les dimensions ont été défini
            if(!$request->request->get('hasDimension')){
                if ($chambre->getDimension()) {
                    $entityManager->remove($chambre->getDimension());
                    $chambre->setDimension(null);
                }
            }else{
                if($chambre->getDimension() && !$chambre->getDimension()->hasValues()) {
                    $chambre->setDimension((null));
                }
            }
            
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('my_things');
        }

        return $this->render('account/pages/thing/chambre/edit.html.twig', [
            'thing' => $chambre,
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}/delete", name="chambre_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Chambre $chambre): Response
    {
        if ($this->isCsrfTokenValid('delete'.$chambre->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($chambre);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::DELETE_MSG);
        }

        return $this->redirectToRoute('my_things');
    }
}
