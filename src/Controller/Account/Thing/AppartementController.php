<?php

namespace App\Controller\Account\Thing;

use App\Entity\Appartement;
use App\Form\Thing\AppartementType;
use App\Repository\Thing\AppartementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\SiteConfig;

/**
 * @Route("/account/appart")
 */
class AppartementController extends AbstractController
{
    /**
     * @Route("/", name="appartement_index", methods={"GET"})
     */
    public function index(AppartementRepository $appartementRepository): Response
    {
        return $this->render('account/pages/thing/appartement/index.html.twig', [
            'appartements' => $appartementRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="appartement_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $appartement = new Appartement();
        $appartement->setOwner($this->getUser());
        $form = $this->createForm(AppartementType::class, $appartement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            //vérification si les dimensions ont été définies
            if(!$request->request->get('hasDimension'))
                $appartement->setDimension(null);


            $entityManager->persist($appartement);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('my_things');
        }

        return $this->render('account/pages/thing/appartement/new.html.twig', [
            'thing' => $appartement,
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}", name="appartement_show", methods={"GET"})
     */
    public function show(Appartement $appartement): Response
    {
        return $this->render('account/pages/thing/appartement/show.html.twig', [
            'thing' => $appartement,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="appartement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Appartement $appartement): Response
    {
        $form = $this->createForm(AppartementType::class, $appartement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            
            //vérification si les dimensions ont été défini
            if(!$request->request->get('hasDimension')){
                if ($appartement->getDimension()) {
                    $entityManager->remove($appartement->getDimension());
                    $appartement->setDimension(null);
                }
            }else{
                if($appartement->getDimension() && !$appartement->getDimension()->hasValues()) {
                    $appartement->setDimension((null));
                }
            }
            
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::SAVE_MSG);

            return $this->redirectToRoute('my_things');
        }

        return $this->render('account/pages/thing/appartement/edit.html.twig', [
            'thing' => $appartement,
            'form' => $form->createView(),
            'select2params' => '{{{q}}}',
        ]);
    }

    /**
     * @Route("/{id}/delete", name="appartement_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Appartement $appartement): Response
    {
        if ($this->isCsrfTokenValid('delete'.$appartement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($appartement);
            $entityManager->flush();
            $this->addFlash('acc_success_msg', SiteConfig::DELETE_MSG);
        }

        return $this->redirectToRoute('my_things');
    }
}
