<?php

namespace App\Controller\Admin\Map;

use App\Entity\Arrondissement;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use App\Controller\Admin\TraitBaseFielsCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;

class ArrondissementCrudController extends AbstractCrudController
{
    use TraitBaseFielsCrudController;

    public static function getEntityFqcn(): string
    {
        return Arrondissement::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('name'),
            SlugField::new('slug')->setRequired(false),
            AssociationField::new('ville'),
            TextEditorField::new('description')
        ];
    }
}
