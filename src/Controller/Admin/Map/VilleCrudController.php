<?php

namespace App\Controller\Admin\Map;

use App\Entity\Ville;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use App\Controller\Admin\TraitBaseFielsCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;

class VilleCrudController extends AbstractCrudController
{
    use TraitBaseFielsCrudController;

    public static function getEntityFqcn(): string
    {
        return Ville::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('name'),
            TextField::new('slug')->setRequired(false),
            AssociationField::new('departement'),
            TextEditorField::new('description')
        ];
    }
    
}
