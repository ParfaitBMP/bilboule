<?php

namespace App\Controller\Admin\Map;

use App\Entity\Departement;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use App\Controller\Admin\TraitBaseFielsCrudController;


class DepartementCrudController extends AbstractCrudController
{
    use TraitBaseFielsCrudController;

    public static function getEntityFqcn(): string
    {
        return Departement::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('name'),
            TextField::new('slug')->setRequired(false),
            AssociationField::new('region')
        ];
    }
    
}
