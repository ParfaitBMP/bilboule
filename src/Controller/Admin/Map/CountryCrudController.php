<?php

namespace App\Controller\Admin\Map;

use App\Entity\Country;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CountryField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use App\Controller\Admin\TraitBaseFielsCrudController;

class CountryCrudController extends AbstractCrudController
{
    use TraitBaseFielsCrudController;

    public static function getEntityFqcn(): string
    {
        return Country::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            CountryField::new('code'),
            'name',
            TextField::new('slug')->setRequired(false),
            'description'
        ];
    }
    
}
