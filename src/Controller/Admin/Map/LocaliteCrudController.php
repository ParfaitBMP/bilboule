<?php

namespace App\Controller\Admin\Map;

use App\Entity\Localite;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use App\Controller\Admin\TraitBaseFielsCrudController;

class LocaliteCrudController extends AbstractCrudController
{
    use TraitBaseFielsCrudController;

    public static function getEntityFqcn(): string
    {
        return Localite::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('name'),
            SlugField::new('slug')->setRequired(false),
            AssociationField::new('arrondissement'),
            TextEditorField::new('description')
        ];
    }
}
