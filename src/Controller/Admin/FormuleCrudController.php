<?php

namespace App\Controller\Admin;

use App\Entity\Formule;
use App\Repository\SiteConfigRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use Vich\UploaderBundle\Form\Type\VichFileType;

class FormuleCrudController extends AbstractCrudController
{
    private $siteConfigRepository;

    public function __construct(SiteConfigRepository $siteConfigRepository){
        $this->siteConfigRepository = $siteConfigRepository;
    }

    public static function getEntityFqcn(): string
    {
        return Formule::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('name', 'Nom'),
            MoneyField::new('amount', 'Montant')->setCurrency('XAF')->setNumDecimals(0),
            IntegerField::new('duration', 'Durée (Jour-s-)'),
            TextField::new('summary', 'Résumé de la formule')->onlyOnIndex(),
            IntegerField::new('sequence', 'Ordre d\'affichage'),
            TextField::new('icon', 'Icône')->onlyOnForms(),
            ColorField::new('color', 'Couleur'),
            UrlField::new('url')->hideOnIndex(),
            TextEditorField::new('description')->onlyOnForms(),
            ImageField::new('imageFile','Image de couverture')
                ->setRequired(true)
                ->setFormType(VichFileType::class)
                ->setFormTypeOptions(['allow_delete' => false, 'by_reference' => false])
                ->onlyOnForms(),             
            BooleanField::new('active'),
        ];
    }
    
}
