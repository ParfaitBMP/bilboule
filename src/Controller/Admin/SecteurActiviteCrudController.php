<?php

namespace App\Controller\Admin;

use App\Entity\SecteurActivite;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use App\Controller\Admin\TraitBaseFielsCrudController;


class SecteurActiviteCrudController extends AbstractCrudController
{
    use TraitBaseFielsCrudController;
    
    public static function getEntityFqcn(): string
    {
        return SecteurActivite::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->showEntityActionsAsDropdown()
            ->setEntityLabelInSingular('Secteur d\'activité')
            ->setEntityLabelInPlural('Secteurs d\'activités')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('name', 'Nom'),
            SlugField::new('slug')->setTargetFieldName('name'),
            TextEditorField::new('description')
        ];
    }
}
