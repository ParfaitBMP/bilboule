<?php

namespace App\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

Trait TraitBaseFielsCrudController
{
    private $slugger;
    private $entityManager;

    function __construct(SluggerInterface $slugger, EntityManagerInterface $entityManager)
    {
        $this->slugger = $slugger;
        $this->entityManager = $entityManager;
    }

	// public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
 //    // public function createEntity(string $entityFqcn)
 //    {
 //        // if($entityInstance->getName())
 //        //     $entityInstance->setSlug($this->slugger->slug($entityInstance->getName()));

 //        if(!$entityInstance->getCreateDate() && !$entityInstance->getCreateUser()){
 //        // $entityInstance = $this->getContext()->getEntity()->getInstance();
 //            $entityInstance->setCreateUser($this->getUser());
 //            $entityInstance->setCreateDate(new \Datetime());
 //        }

 //        $this->entityManager->persist($entityInstance);
 //        $this->entityManager->flush();
 //    }

 //    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
 //    {
 //        $entityInstance->setWriteUser($this->getUser());
 //        $entityInstance->setWriteDate(new \Datetime());

 //        // if(!$entityInstance->getSlug() && $entityInstance->getName())
 //        //     $entityInstance->setSlug($this->slugger->slug($entityInstance->getName()));
 //        // die('dezfe');
 //        $entityManager->persist($entityInstance);
 //        $entityManager->flush();
 //    }
}