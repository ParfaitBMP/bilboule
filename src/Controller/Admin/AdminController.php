<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use App\Entity\Country;
use App\Entity\Region;
use App\Entity\Departement;
use App\Entity\Arrondissement;
use App\Entity\Ville;
use App\Entity\Localite;
use App\Entity\Place;
use App\Entity\Annonce;
use App\Entity\AnnonceMaison;
use App\Entity\AnnonceAppartement;
use App\Entity\AnnonceStudio;
use App\Entity\AnnonceChambre;
use App\Entity\AnnonceTerrain;
use App\Entity\AnnonceImmeuble;
use App\Entity\ExtraImmeuble;
use App\Entity\ExtraLogement;
use App\Entity\ExtraTerrain;
use App\Entity\Maison;
use App\Entity\Appartement;
use App\Entity\Studio;
use App\Entity\Chambre;
use App\Entity\Terrain;
use App\Entity\Immeuble;
use App\Entity\User;
use App\Entity\Formule;
use App\Entity\Category;
use App\Entity\SiteConfig;
use App\Entity\SecteurActivite;

class AdminController extends AbstractDashboardController
{
    /**
     * @Route("/secure-admin", name="admin_dashboard")
     */
    public function index(): Response
    {
        return parent::index();
        // $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        // return $this->redirect($routeBuilder->setController(RegionCrudController::class)->generateUrl());

        // // you can also redirect to different pages depending on the current user
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // // you can also render some template to display a proper Dashboard
        // // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        // return $this->render('some/path/my-dashboard.html.twig');

    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Bilboule Administration');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Tableau de bord', 'fa fa-chalkboard-teacher');
        // Gesion des annonces
        yield MenuItem::section('Gesion des annonces');
        // Elements
        yield MenuItem::subMenu('Eléments', 'fa fa-question')
                ->setSubItems([
                    MenuItem::linkToCrud('Maisons', 'fa fa-home', Maison::class),
                    MenuItem::linkToCrud('Appartements', 'fa fa-home', Appartement::class),
                    MenuItem::linkToCrud('Studios', 'fa fa-door-open', Studio::class),
                    MenuItem::linkToCrud('Chambres', 'fa fa-bed', Chambre::class),
                    MenuItem::linkToCrud('Terrains', 'fa fa-map-marked-alt', Terrain::class),
                    MenuItem::linkToCrud('Immeubles', 'fa fa-building', Immeuble::class)
                ]);
        // Annonces
        yield MenuItem::subMenu('Annonces', 'fa fa-bullhorn')
                ->setSubItems([
                    MenuItem::linkToCrud('Toutes', 'fa fa-list', Annonce::class)->setAction('index'),
                    MenuItem::linkToCrud('Maisons', 'fa fa-home', AnnonceMaison::class),
                    MenuItem::linkToCrud('Appartements', 'fa fa-home', AnnonceAppartement::class),
                    MenuItem::linkToCrud('Studios', 'fa fa-door-open', AnnonceStudio::class),
                    MenuItem::linkToCrud('Chambres', 'fa fa-bed', AnnonceChambre::class),
                    MenuItem::linkToCrud('Terrains', 'fa fa-map-marked-alt', AnnonceTerrain::class),
                    MenuItem::linkToCrud('Immeubles', 'fa fa-building', AnnonceImmeuble::class)
                ]);
        yield MenuItem::subMenu('Extras', 'fa fa-star')
                ->setSubItems([
                    MenuItem::linkToCrud('Logement', 'fa fa-home', ExtraLogement::class),
                    MenuItem::linkToCrud('Immeuble', 'fa fa-building', ExtraImmeuble::class),
                    MenuItem::linkToCrud('Terrain', 'fa fa-map-marked-alt', ExtraTerrain::class)
                ]);
        // Paramétrage
        yield MenuItem::section('Paramétrage');
        yield MenuItem::linkToCrud('Formules', 'fa fa-award', Formule::class);
        yield MenuItem::linkToCrud('Categories', 'fa fa-sitemap', Category::class);
        yield MenuItem::linkToCrud('Secteurs d\'activités', 'fa fa-briefcase', SecteurActivite::class);
        // Données géographiques
        yield MenuItem::section('Données géographiques');
        yield MenuItem::linkToCrud('Pays', 'fa fa-flag', Country::class);
        yield MenuItem::linkToCrud('Regions', 'fa fa-globe-africa', Region::class);
        yield MenuItem::linkToCrud('Departements', 'fa fa-map', Departement::class);
        yield MenuItem::linkToCrud('Villes', 'fa fa-city', Ville::class);
        yield MenuItem::linkToCrud('Arrondissements', 'fa fa-map-signs', Arrondissement::class);
        yield MenuItem::linkToCrud('Localités', 'fa fa-map-marker-alt', Localite::class);
        yield MenuItem::linkToCrud('Tous les lieux', 'fa fa-street-view', Place::class);
        //Configuration du site
        yield MenuItem::section('Système');
        yield MenuItem::linkToCrud('Configuration', 'fa fa-tools', SiteConfig::class);
        yield MenuItem::linkToCrud('Utilisateurs', 'fa fa-users', User::class);
    }

    public function configureActions(): Actions
    {
       return  parent::configureActions()
            ->add(Crud::PAGE_NEW, Action::INDEX)
            ->update(Crud::PAGE_NEW, Action::INDEX, function (Action $action) {
                        return $action
                            ->setIcon('fas fa-list')
                            ->setLabel('Afficher la liste')
                            ->addCssClass('btn btn-light');
            })
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->update(Crud::PAGE_EDIT, Action::INDEX, function (Action $action) {
                        return $action
                            ->setIcon('fas fa-list')
                            ->setLabel('Afficher la liste')
                            ->addCssClass('btn btn-light');
            })
       ;
    }
}
