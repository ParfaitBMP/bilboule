<?php

namespace App\Controller\Admin\Ad;

use App\Entity\Annonce;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;

Trait TraitAnnonceCrudBase
{
	
    public function configureActions(Actions $actions): Actions
    {
        $publishAd = Action::new('PUBLISH', 'Publier', 'fa fa-paper-plane')
            ->linkToCrudAction('setToPublishAction')
            ->addCssClass('btn btn-success')
            ->displayIf(static function ($entity) {
                return $entity->isDraft();
            });
            
        $validateAd = Action::new('VALIDATE', 'Valider', 'fa fa-done')
            ->linkToCrudAction('setToDoneAction')
            ->addCssClass('btn btn-success')
            ->displayIf(static function ($entity) {
                return $entity->isPending() || $entity->isCanceled();
            });

        $unPublishAd = Action::new('UNPUBLISH', 'Dépublier', 'fa fa-stop-circle')
            ->linkToCrudAction('setToDraftAction')
            ->addCssClass('btn btn-warning')
            ->displayIf(static function ($entity) {
                return $entity->isPublished();
            });

        $cancelAd = Action::new('CANCEL', 'Annuler', 'fa fa-ban')
            ->linkToCrudAction('setToCancelAction')
            ->addCssClass('btn btn-danger')
            ->displayIf(static function ($entity) {
                return $entity->isPending() || $entity->isPublished();
            });

        return $actions
            // INDEX
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->reorder(Crud::PAGE_INDEX, [Action::DETAIL, Action::EDIT, Action::DELETE])
            // DETAIL
            ->add(Crud::PAGE_DETAIL, $publishAd)
            ->add(Crud::PAGE_DETAIL, $validateAd)
            ->add(Crud::PAGE_DETAIL, $unPublishAd)
            ->add(Crud::PAGE_DETAIL, $cancelAd)
            // EDIT
            ->add(Crud::PAGE_EDIT, $publishAd)
            ->add(Crud::PAGE_EDIT, $validateAd)
            ->add(Crud::PAGE_EDIT, $unPublishAd)
            ->add(Crud::PAGE_EDIT, $cancelAd)
        ;
    }

    // Publier une annonce
    public function setToPublishAction(AdminContext $context){
        $entity = $context->getEntity()->getInstance()->setToPending();
        $manager = $this->getDoctrine()->getManagerForClass(Annonce::class);
        $manager->persist($entity);
        $manager->flush();

        $url = $context->getReferrer()
                    ?? $this->get(CrudUrlGenerator::class)->build()->setAction(Action::INDEX)->generateUrl();

        return $this->redirect($url);
    }
    
    // Valider une annonce
    public function setToDoneAction(AdminContext $context){
        $entity = $context->getEntity()->getInstance()->setToDone();
        $manager = $this->getDoctrine()->getManagerForClass(Annonce::class);
        $manager->persist($entity);
        $manager->flush();

        $url = $context->getReferrer()
                    ?? $this->get(CrudUrlGenerator::class)->build()->setAction(Action::INDEX)->generateUrl();

        return $this->redirect($url);
    }

    // Mettre l'annonce à l'état brouillon
    public function setToDraftAction(AdminContext $context){
        $entity = $context->getEntity()->getInstance()->setToDraft();
        $manager = $this->getDoctrine()->getManagerForClass(Annonce::class);
        $manager->persist($entity);
        $manager->flush();
    }

    // Annuler l'annonce
    public function setToCancelAction(AdminContext $context){
        $entity = $context->getEntity()->getInstance()->setToCancel();
        $manager = $this->getDoctrine()->getManagerForClass(Annonce::class);
        $manager->persist($entity);
        $manager->flush();
    }
}