<?php

namespace App\Controller\Admin\Ad;

use App\Entity\Annonce;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

class AnnonceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Annonce::class;
    }
    
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->showEntityActionsAsDropdown()
            ->setEntityLabelInSingular('Annonce')
            ->setEntityLabelInPlural('Annonces')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            'id',
            'name',
            DateTimeField::new('publishDate', 'Date validation'),
            IntegerField::new('duration', 'Durée')
                ->setHelp('Nombre de jours pendant lesquels votre annonce sera diffusée sur la plateforme')
                ->setFormTypeOption('attr', ['min' => 5])
                ->formatValue(function ($value, $entity) {
                    return $entity->getDuration() . ' Jours';
                }),
            DateTimeField::new('expireDate', 'Date expiration'),
            AssociationField::new('category', 'Catégorie'),
            AssociationField::new('formule'),
            'state'
        ];
    }
    
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->remove(Crud::PAGE_INDEX, Action::EDIT)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
        ;
    }
}
