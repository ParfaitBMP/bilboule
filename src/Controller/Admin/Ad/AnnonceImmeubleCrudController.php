<?php

namespace App\Controller\Admin\Ad;

use App\Entity\AnnonceImmeuble;
use App\Entity\Immeuble;
use App\Entity\Category;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use App\Form\Thing\ImmeubleType;
use App\Controller\Admin\TraitBaseFielsCrudController;
use App\Controller\Admin\Ad\TraitAnnonceCrudBase;
use App\Repository\CategoryRepository;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Exception\ForbiddenActionException;
use EasyCorp\Bundle\EasyAdminBundle\Exception\InsufficientEntityPermissionException;
use EasyCorp\Bundle\EasyAdminBundle\Factory\EntityFactory;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Security\Permission;

class AnnonceImmeubleCrudController extends AbstractCrudController
{
    use TraitAnnonceCrudBase;
    use TraitBaseFielsCrudController;

    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository){
        $this->categoryRepository = $categoryRepository;
    }

    public static function getEntityFqcn(): string
    {
        return AnnonceImmeuble::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->showEntityActionsAsDropdown()
            ->setEntityLabelInSingular('Annonce - immeuble')
            ->setEntityLabelInPlural('Annonces d\'immeubles')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $fields =  [
            FormField::addPanel("Informations de base sur l'annonce")
                ->setIcon('fa fa-bullhorn'),
            IdField::new('id')->onlyOnIndex(),
            TextField::new('name', 'Titre de l\'annonce'),
            SlugField::new('slug')->setTargetFieldName('name')->onlyOnForms(),
            AssociationField::new('category')
                ->setFormTypeOption('required', true)
                ->setFormTypeOption('disabled', true),
            IntegerField::new('duration', 'Durée')
                ->setHelp('Nombre de jours pendant lesquels votre annonce sera diffusée sur la plateforme')
                ->setFormTypeOption('attr', ['min' => 5])
                ->formatValue(function ($value, $entity) {
                    return $entity->getDuration() . ' Jours';
                }),
            MoneyField::new('amountDay', 'Montant journalier')
                ->setHelp('Coût de l\'annonce par jour')
                ->setFormTypeOption('attr', ['min' => 100])
                ->setCurrency('XAF'),
            AssociationField::new('formule', 'Formule')->setFormTypeOption('required', false),
            FormField::addPanel('Informations sur l\'immeuble')
                ->setIcon('fa fa-home')
                ->setHelp('Informations sur la propriété'),
            TextField::new('immeuble', 'Infos sur la propriété')
                ->setFormType(ImmeubleType::class)->setFormTypeOption('disabled', true),
            TextField::new('state')->onlyOnIndex()
        ];
       
       return $fields;
    }

    public function createEntity(string $entityFqcn)
    {
        $category = $this->categoryRepository->findOneByCode(Category::CODE_BUILDING);
        $entity = $category? (new $entityFqcn())->setCategory($category) : (new $entityFqcn());

        return $entity;
    }

    public function new(AdminContext $context)
    {
        $event = new BeforeCrudActionEvent($context);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        if (!$this->isGranted(Permission::EA_EXECUTE_ACTION)) {
            throw new ForbiddenActionException($context);
        }

        if (!$context->getEntity()->isAccessible()) {
            throw new InsufficientEntityPermissionException($context);
        }

        $context->getEntity()->setInstance($this->createEntity($context->getEntity()->getFqcn()));
        $this->get(EntityFactory::class)->processFields($context->getEntity(), FieldCollection::new($this->configureFields(Crud::PAGE_NEW)));
        $this->get(EntityFactory::class)->processActions($context->getEntity(), $context->getCrud()->getActionsConfig());
        $entityInstance = $context->getEntity()->getInstance();

        $entityInstance->setImmeuble($this->getDoctrine()->getRepository(Immeuble::class)->find($context->getRequest()->get('thing')));
       
        $newForm = $this->createNewForm($context->getEntity(), $context->getCrud()->getNewFormOptions(), $context);
        $newForm->handleRequest($context->getRequest());
        if ($newForm->isSubmitted() && $newForm->isValid()) {
            // TODO:
            // $this->processUploadedFiles($newForm);

            $event = new BeforeEntityPersistedEvent($entityInstance);
            $this->get('event_dispatcher')->dispatch($event);
            $entityInstance = $event->getEntityInstance();

            $this->persistEntity($this->get('doctrine')->getManagerForClass($context->getEntity()->getFqcn()), $entityInstance);

            $this->get('event_dispatcher')->dispatch(new AfterEntityPersistedEvent($entityInstance));
            $context->getEntity()->setInstance($entityInstance);

            $submitButtonName = $context->getRequest()->request->get('ea')['newForm']['btn'];
            if (Action::SAVE_AND_CONTINUE === $submitButtonName) {
                $url = $this->get(CrudUrlGenerator::class)->build()
                    ->setAction(Action::EDIT)
                    ->setEntityId($context->getEntity()->getPrimaryKeyValue())
                    ->generateUrl();

                return $this->redirect($url);
            }

            if (Action::SAVE_AND_RETURN === $submitButtonName) {
                $url = $context->getReferrer()
                    ?? $this->get(CrudUrlGenerator::class)->build()->setAction(Action::INDEX)->generateUrl();

                return $this->redirect($url);
            }

            if (Action::SAVE_AND_ADD_ANOTHER === $submitButtonName) {
                $url = $this->get(CrudUrlGenerator::class)->build()->setAction(Action::NEW)->generateUrl();

                return $this->redirect($url);
            }

            return $this->redirectToRoute($context->getDashboardRouteName());
        }

        $responseParameters = $this->configureResponseParameters(KeyValueStore::new([
            'pageName' => Crud::PAGE_NEW,
            'templateName' => 'crud/new',
            'entity' => $context->getEntity(),
            'new_form' => $newForm,
        ]));

        $event = new AfterCrudActionEvent($context, $responseParameters);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        return $responseParameters;
    }
}