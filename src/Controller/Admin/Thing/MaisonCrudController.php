<?php

namespace App\Controller\Admin\Thing;

use App\Entity\Maison;
use App\Controller\Admin\Map\PlaceCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use App\Form\PriceType;
use App\Form\DimensionType;
use App\Form\PJ\PjMaisonType;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use App\Controller\Admin\Ad\AnnonceMaisonCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;

class MaisonCrudController extends AbstractCrudController
{
    use TraitProprieteCrud;
    
    private $crudUrlGenerator;

    public function __construct(CrudUrlGenerator $crudUrlGenerator) {
        $this->crudUrlGenerator = $crudUrlGenerator;
    }
    
    public static function getEntityFqcn(): string
    {
        return Maison::class;
    }

     public function configureCrud(Crud $crud): Crud
     {
         return $crud
            ->showEntityActionsAsDropdown()
            ->setEntityLabelInSingular('Maison')
            ->setEntityLabelInPlural('Maisons')
        ;
     }
    
    public function configureFields(string $pageName): iterable
    {
        $fields = [
            FormField::addPanel("Informations générales sur: maison - villa - duplex")
            ->setHelp("Informations générales sur votre propriété"),
                IdField::new('id')->onlyOnIndex(),
                TextField::new('name')->setRequired(true),
                AssociationField::new('owner', 'Propiétaire')
                    ->setRequired(true)
                    ->autocomplete(),
                ChoiceField::new('operation')
                    ->setChoices([
                        'Location' => 'LOCATION',
                        'Vente' => 'VENTE'
                    ]),
                DateField::new('dateDisponibilite')->onlyOnForms(),
                DateField::new('dateDisponibilite')->onlyOnDetail(),
                IntegerField::new('nbrEtage')->onlyOnForms(),
                IntegerField::new('nbrGarage')->onlyOnForms(),
                IntegerField::new('nbrSalon')->onlyOnForms(),
                IntegerField::new('nbrChambre')->onlyOnForms(),
                IntegerField::new('nbrCuisine')->onlyOnForms(),
                IntegerField::new('nbrSalleDeBain')->onlyOnForms(),
                TextEditorField::new('description')->onlyOnForms(),
            // Localisation
            FormField::addPanel("Localisation")
            ->setHelp("Indiquez où se trouve votre propriété"),
                    AssociationField::new('localisation')
                        ->setCrudController(PlaceCrudController::class)
                        ->autocomplete(),
                    TextEditorField::new('localisation.comment', 'Description de l\'emplacement')
                        ->onlyWhenUpdating(),
            // Prix
            FormField::addPanel("Prix")
            ->setHelp("Indiquez le prix de votre propriété pour l'opération"),
                    TextField::new('price', 'Prix')
                        ->setFormType(PriceType::class)
                        ->formatValue(function ($value, $entity) {
                            return $entity->getPrice()->getAmount() . ' FCFA';
                        }),
            // Dimensions
            FormField::addPanel("Dimensions")
            ->setHelp("Veuillez renseigner les dimensions de votre propriété (Laisser vide pour ignorer)"),
                    TextField::new('dimension')
                        ->setFormType(DimensionType::class)
                        ->setRequired(false)
                        ->onlyOnForms(),
            // Photos
            FormField::addPanel('Photos de la maison')
            ->setIcon('fa fa-camera-retro')
            ->setHelp('Quelques photos associées à votre annonce et présentant la maison')
        ];
        
        // Phootos - suite
        if (Crud::PAGE_DETAIL === $pageName) {
            $fields[] = CollectionField::new('piecesJointes')
                        ->setTemplatePath('admin/vich/images.html.twig')
                        ->onlyOnDetail();
        }elseif(in_array($pageName, [Crud::PAGE_NEW, Crud::PAGE_EDIT])) {
            $fields[] = CollectionField::new('piecesJointes', 'Photos')
                        ->setEntryType(PjMaisonType::class)
                        ->setFormTypeOption('by_reference', false)
                        ->onlyOnForms();
        }

        // Autres
        array_push($fields, 
            FormField::addPanel('Autres options')
            ->setIcon('fa fa-cogs')
            ->setHelp("Précicez si votre propriétés dispose d'autres avantages")
        );
        
        if (in_array($pageName, [Crud::PAGE_NEW, Crud::PAGE_EDIT, Crud::PAGE_DETAIL])) {
            $fields[] = AssociationField::new('extraLogements');
        }
        array_push($fields, BooleanField::new('active', 'Actif'));
        
        return $fields;
    }
    
    public function configureActions(Actions $actions): Actions
    {
        $newAd = Action::new('NEW_AD', 'Nouvelle annonce', 'fa fa-add')
            ->linkToCrudAction('createAd')
            ->displayIf(static function ($entity) {
                return $entity->getId();
            });
            
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
                
            //custom action
            ->add(Crud::PAGE_INDEX, $newAd)
            ->add(Crud::PAGE_DETAIL, $newAd)
            ->add(Crud::PAGE_EDIT, $newAd)
        ;
    }
    
    public function createAd(AdminContext $context) {
        $url = $this->crudUrlGenerator
            ->build()
            ->setController(AnnonceMaisonCrudController::class)
            ->setAction(Action::NEW)
                ->setEntityId(null)
            ->set('thing', $context->getEntity()->getInstance()->getId())
            ->generateUrl();
        
        return $this->redirect($url);
    }
}
