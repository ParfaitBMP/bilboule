<?php

namespace App\Controller\Admin\Thing;

use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Parfait
 */
Trait TraitProprieteCrud 
{
    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        if (!$entityInstance->getDimension()->hasValues()) {
            $entityInstance->setDimension(null);
        }

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }
}
