<?php

namespace App\Controller\Admin\Extra;

use App\Entity\ExtraTerrain;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ExtraTerrainCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ExtraTerrain::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->showEntityActionsAsDropdown()
            ->setEntityLabelInSingular('Extra terrain')
            ->setEntityLabelInPlural('Extras terrains')
        ;
    }
    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('name'),
            TextField::new('icon'),
            TextField::new('url')
        ];
    }
}
