<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('public/pages/home.html.twig');
    }

    /**
     * @Route("/search", name="search")
     */
    public function searchList()
    {
        return $this->render('public/pages/search_list.html.twig');
    }

    /**
     * @Route("/annonce", name="ad_detail")
     */
    public function AdDetail()
    {
        return $this->render('public/pages/ad_detail.html.twig');
    }
}
