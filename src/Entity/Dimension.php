<?php

namespace App\Entity;

use App\Repository\DimensionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DimensionRepository::class)
 */
class Dimension
{
    use TraitBaseEntity;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $availableArea;

    /**
     * @ORM\Column(type="float")
     */
    private $livingArea;

    /**
     * @ORM\Column(type="float")
     */
    private $totalArea;

    /**
     * @ORM\OneToOne(targetEntity=Property::class, mappedBy="dimension", cascade={"persist"})
     */
    private $property;

    public function __toString(): ?string
    {
        return 'Disponible: '. $this->availableArea .'m2, Occupable: '. $this->livingArea .'m2, Total: '. $this->totalArea .'m2';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAvailableArea(): ?float
    {
        return $this->availableArea;
    }

    public function setAvailableArea(?float $availableArea): self
    {
        $this->availableArea = $availableArea;

        return $this;
    }

    public function getLivingArea(): ?float
    {
        return $this->livingArea;
    }

    public function setLivingArea(?float $livingArea): self
    {
        $this->livingArea = $livingArea;

        return $this;
    }

    public function getTotalArea(): ?float
    {
        return $this->totalArea;
    }

    public function setTotalArea(float $totalArea): self
    {
        $this->totalArea = $totalArea;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(Property $property): self
    {
        $this->property = $property;

        // set the owning side of the relation if necessary
        if ($property->getDimension() !== $this) {
            $property->setDimension($this);
        }

        return $this;
    }

    public function hasValues(): ?bool
    {
        return $this->availableArea || $this->livingArea || $this->totalArea;
    }
}
