<?php

namespace App\Entity;

use App\Repository\Map\DepartementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DepartementRepository::class)
 */
class Departement extends Place
{
    /**
     * @ORM\OneToMany(targetEntity=Ville::class, mappedBy="departement")
     */
    private $villes;

    /**
     * @ORM\ManyToOne(targetEntity=Region::class, inversedBy="departements")
     * @ORM\JoinColumn(nullable=true)
     */
    private $region;

    public function __construct()
    {
        $this->villes = new ArrayCollection();
    }
    
    public function __toString(): ?string
    {
        return parent::getName();
    }

    public function getTree(): ?string
    {
        return $this->region->getTree() .', '. $this->region->getName();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    /**
     * @return Collection|Ville[]
     */
    public function getVilles(): Collection
    {
        return $this->villes;
    }

    public function addVille(Ville $ville): self
    {
        if (!$this->villes->contains($ville)) {
            $this->villes[] = $ville;
            $ville->setDepartement($this);
        }

        return $this;
    }

    public function removeVille(Ville $ville): self
    {
        if ($this->villes->contains($ville)) {
            $this->villes->removeElement($ville);
            // set the owning side to null (unless already changed)
            if ($ville->getDepartement() === $this) {
                $ville->setDepartement(null);
            }
        }

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): self
    {
        $this->region = $region;

        return $this;
    }
}
