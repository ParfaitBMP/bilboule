<?php

namespace App\Entity;

use App\Repository\Map\LocalisationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LocalisationRepository::class)
 */
class Localisation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Place::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $place;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\OneToOne(targetEntity=SomeThing::class, mappedBy="localisation", cascade={"persist", "remove"})
     */
    private $someThing;

    public function __toString(): ?string
    {
        return $this->place;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlace(): ?Place
    {
        return $this->place;
    }

    public function setPlace(?Place $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
    
    public function getSomeThing(): ?SomeThing
    {
        return $this->someThing;
    }

    public function setSomeThing(SomeThing $someThing): self
    {
        $this->someThing = $someThing;

        // set the owning side of the relation if necessary
        if ($someThing->getLocalisation() !== $this) {
            $someThing->setLocalisation($this);
        }

        return $this;
    }
}
