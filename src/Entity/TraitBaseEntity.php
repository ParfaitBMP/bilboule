<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/*
 * @ORM\HasLifecycleCallbacks()
 */
trait TraitBaseEntity
{
    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createDate;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $createUser;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $writeDate;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $writeUser;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default": true})
     */
    private $active = true;

    public function __construct()
    {
        $this->active = true;
    }

    public function getCreateDate(): ?\DateTimeInterface
    {
        return $this->createDate;
    }

    public function setCreateDate(?\DateTimeInterface $createDate): self
    {
        $this->createDate = $createDate;

        return $this;
    }

    public function getCreateUser(): ?User
    {
        return $this->createUser;
    }

    public function setCreateUser(?User $createUser): self
    {
        $this->createUser = $createUser;

        return $this;
    }

    public function getWriteDate(): ?\DateTimeInterface
    {
        return $this->writeDate;
    }

    public function setWriteDate(?\DateTimeInterface $writeDate): self
    {
        $this->writeDate = $writeDate;

        return $this;
    }

    public function getWriteUser(): ?User
    {
        return $this->writeUser;
    }

    public function setWriteUser(?User $writeUser): self
    {
        $this->writeUser = $writeUser;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
    
    /*
     * ORM\PrePersist
     */
    function createTrigger() {
        if(!$this->createDate){
            $this->createDate = new \DateTime();
        }
    }
    
    /*
     * ORM\PreUpdate
     */
    function writeTrigger() {
        $this->writeDate = new \DateTime();
    }
}
