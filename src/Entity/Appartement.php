<?php

namespace App\Entity;

use App\Repository\Thing\AppartementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=AppartementRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class Appartement extends Logement
{
    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $nbrEtage;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $nbrGarage;

    /**
     * @ORM\OneToMany(targetEntity=AnnonceAppartement::class, mappedBy="appartement")
     */
    private $annonces;

    /**
     * @ORM\OneToMany(targetEntity=PjAppartement::class, mappedBy="appartement", cascade={"persist", "remove"})
     */
    private $piecesJointes;

    /**
     * @ORM\ManyToMany(targetEntity=ExtraLogement::class)
     */
    private $extraLogements;

    public function __construct()
    {
        parent::__construct();
        $this->annonces = new ArrayCollection();
        $this->piecesJointes = new ArrayCollection();
        $this->extraLogements = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return parent::__toString();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getNbrEtage(): ?int
    {
        return $this->nbrEtage;
    }

    public function setNbrEtage(?int $nbrEtage): self
    {
        $this->nbrEtage = $nbrEtage;

        return $this;
    }

    public function getNbrGarage(): ?int
    {
        return $this->nbrGarage;
    }

    public function setNbrGarage(?int $nbrGarage): self
    {
        $this->nbrGarage = $nbrGarage;

        return $this;
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annonces[] = $annonce;
            $annonce->setAppartement($this);
        }

        return $this;
    }

    public function removeAnnonce(Annonce $annonce): self
    {
        if ($this->annonces->contains($annonce)) {
            $this->annonces->removeElement($annonce);
            // set the owning side to null (unless already changed)
            if ($annonce->getAppartement() === $this) {
                $annonce->setAppartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PjAppartement[]
     */
    public function getPiecesJointes(): Collection
    {
        return $this->piecesJointes;
    }

    public function addPiecesJointe(PjAppartement $piecesJointe): self
    {
        if (!$this->piecesJointes->contains($piecesJointe)) {
            $this->piecesJointes[] = $piecesJointe;
            $piecesJointe->setAppartement($this);
        }

        return $this;
    }

    public function removePiecesJointe(PjAppartement $piecesJointe): self
    {
        if ($this->piecesJointes->contains($piecesJointe)) {
            $this->piecesJointes->removeElement($piecesJointe);
            // set the owning side to null (unless already changed)
            if ($piecesJointe->getAppartement() === $this) {
                $piecesJointe->setAppartement(null);
            }
        }

        return $this;
    }

    public function getIndexRoute()
    {
        return 'appartement_index';
    }

    public function getNewRoute()
    {
        return 'appartement_new';
    }

    public function getShowRoute()
    {
        return 'appartement_show';
    }

    public function getEditRoute()
    {
        return 'appartement_edit';
    }

    /**
     * @return Collection|ExtraLogement[]
     */
    public function getExtraLogements(): Collection
    {
        return $this->extraLogements;
    }

    public function addExtraLogement(ExtraLogement $extraLogement): self
    {
        if (!$this->extraLogements->contains($extraLogement)) {
            $this->extraLogements[] = $extraLogement;
        }

        return $this;
    }

    public function removeExtraLogement(ExtraLogement $extraLogement): self
    {
        if ($this->extraLogements->contains($extraLogement)) {
            $this->extraLogements->removeElement($extraLogement);
        }

        return $this;
    }
}
