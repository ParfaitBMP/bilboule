<?php

namespace App\Entity;

use App\Repository\Map\CountryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CountryRepository::class)
 */
class Country extends Place
{

    /**
     * @ORM\OneToMany(targetEntity=Region::class, mappedBy="country")
     */
    private $regions;


    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->places = new ArrayCollection();
        $this->regions = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return '['. parent::getCode() .'] '. parent::getName();
    }


    public function getTree(): ?string
    {
        return '';
    }

    public function getId(): ?int
    {
        return parent::getId();
    }
    
    /**
     * @return Collection|Region[]
     */
    public function getRegions(): Collection
    {
        return $this->regions;
    }

    public function addRegion(Region $region): self
    {
        if (!$this->regions->contains($region)) {
            $this->regions[] = $region;
            $region->setCountry($this);
        }

        return $this;
    }

    public function removeRegion(Region $region): self
    {
        if ($this->regions->contains($region)) {
            $this->regions->removeElement($region);
            // set the owning side to null (unless already changed)
            if ($region->getCountry() === $this) {
                $region->setCountry(null);
            }
        }

        return $this;
    }
}
