<?php

namespace App\Entity;

use App\Repository\SiteConfigRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SiteConfigRepository::class)
 */
class SiteConfig
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"default": "XAF"})
     */
    private $currencyCode;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default": 2})
     */
    private $decimalPrecision;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString(): ?string
    {
        return '['.$this->id.']';
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(?string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    public function getDecimalPrecision(): ?int
    {
        return $this->decimalPrecision;
    }

    public function setDecimalPrecision(int $decimalPrecision): self
    {
        $this->decimalPrecision = $decimalPrecision;

        return $this;
    }
}
