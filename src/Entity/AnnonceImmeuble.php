<?php

namespace App\Entity;

use App\Repository\Ad\AnnonceImmeubleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnnonceImmeubleRepository::class)
 */
class AnnonceImmeuble extends Annonce
{
    /**
     * @ORM\ManyToOne(targetEntity=Immeuble::class, inversedBy="annonces", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $immeuble;

    public function __construct()
    {
        parent::__construct();
    }

    public function __toString(): ?string
    {
        return parent::__toString();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getImmeuble(): ?Immeuble
    {
        return $this->immeuble;
    }

    public function setImmeuble(?Immeuble $immeuble): self
    {
        $this->immeuble = $immeuble;

        return $this;
    }

    public function getType()
    {
        return 'IMMEUBLE';
    }

    public function getIndexRoute()
    {
        return 'annonce_immeuble_index';
    }

    public function getNewRoute()
    {
        return 'annonce_immeuble_new';
    }

    public function getShowRoute()
    {
        return 'annonce_immeuble_show';
    }

    public function getEditRoute()
    {
        return 'annonce_immeuble_edit';
    }
}
