<?php

namespace App\Entity;

use App\Repository\Ad\AnnonceMaisonRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnnonceMaisonRepository::class)
 */
class AnnonceMaison extends Annonce
{
    /**
     * @ORM\ManyToOne(targetEntity=Maison::class, inversedBy="annonces", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $maison;

    public function __construct()
    {
        parent::__construct();
    }


    public function __toString(): ?string
    {
        return parent::__toString();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getMaison(): ?Maison
    {
        return $this->maison;
    }

    public function setMaison(?Maison $maison): self
    {
        $this->maison = $maison;

        return $this;
    }

    public function getType()
    {
        return 'MAISON';
    }

    public function getIndexRoute()
    {
        return 'annonce_maison_index';
    }

    public function getNewRoute()
    {
        return 'annonce_maison_new';
    }

    public function getShowRoute()
    {
        return 'annonce_maison_show';
    }

    public function getEditRoute()
    {
        return 'annonce_maison_edit';
    }
}
