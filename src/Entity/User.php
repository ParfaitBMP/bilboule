<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap
    ({
        "PARTICULIER" = "Particulier", 
        "ENTREPRISE" = "Entreprise", 
        "ADMIN" = "UserAdmin",
        "SUPER_ADMIN" = "User"
    })  
 */
class User implements UserInterface
{
    use TraitBaseEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    private $plainPassword; //Pour reset password
    
    private $oldPassword; //Pour reset password

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="userFrom")
     */
    private $createdNotes;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="userTo")
     */
    private $receivedNotes;

    /**
     * @ORM\ManyToOne(targetEntity=Place::class, inversedBy="users")
     */
    private $place;

    /**
     * @ORM\ManyToMany(targetEntity=SecteurActivite::class, inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     */
    private $secteurActivites;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $presentation;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default": false})
     */
    private $certified;

    /**
     * @ORM\OneToMany(targetEntity=SomeThing::class, mappedBy="owner")
     */
    private $someThings;

    public function __construct()
    {
        $this->createdNotes = new ArrayCollection();
        $this->receivedNotes = new ArrayCollection();
        $this->annonces = new ArrayCollection();
        $this->secteurActivites = new ArrayCollection();
        $this->certified = false;
        $this->someThings = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->email;
    }

    protected function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getCreatedNotes(): Collection
    {
        return $this->createdNotes;
    }

    public function addCreatedNote(Note $createdNote): self
    {
        if (!$this->createdNotes->contains($createdNote)) {
            $this->createdNotes[] = $createdNote;
            $createdNote->setUserFrom($this);
        }

        return $this;
    }

    public function removeCreatedNote(Note $createdNote): self
    {
        if ($this->createdNotes->contains($createdNote)) {
            $this->createdNotes->removeElement($createdNote);
            // set the owning side to null (unless already changed)
            if ($createdNote->getUserFrom() === $this) {
                $createdNote->setUserFrom(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getReceivedNotes(): Collection
    {
        return $this->receivedNotes;
    }

    public function addReceivedNote(Note $receivedNote): self
    {
        if (!$this->receivedNotes->contains($receivedNote)) {
            $this->receivedNotes[] = $receivedNote;
            $receivedNote->setUserTo($this);
        }

        return $this;
    }

    public function removeReceivedNote(Note $receivedNote): self
    {
        if ($this->receivedNotes->contains($receivedNote)) {
            $this->receivedNotes->removeElement($receivedNote);
            // set the owning side to null (unless already changed)
            if ($receivedNote->getUserTo() === $this) {
                $receivedNote->setUserTo(null);
            }
        }

        return $this;
    }

    public function getPlace(): ?Place
    {
        return $this->place;
    }

    public function setPlace(?Place $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getType()
    {
        return 'SUPER_ADMIN';
    }

    /**
     * @return Collection|SecteurActivite[]
     */
    public function getSecteurActivites(): Collection
    {
        return $this->secteurActivites;
    }

    public function addSecteurActivite(SecteurActivite $secteurActivite): self
    {
        if (!$this->secteurActivites->contains($secteurActivite)) {
            $this->secteurActivites[] = $secteurActivite;
        }

        return $this;
    }

    public function removeSecteurActivite(SecteurActivite $secteurActivite): self
    {
        if ($this->secteurActivites->contains($secteurActivite)) {
            $this->secteurActivites->removeElement($secteurActivite);
        }

        return $this;
    }

    public function getMyNote(): float
    {
        $myNote = 0.0;
        $somme = 0.0;

        foreach ($this->getReceivedNotes() as $note) {
            $somme += $note->getValue();
        }

        $myNote = count($this->getReceivedNotes())? $somme / count($this->getReceivedNotes()) : 0.0;

        return $myNote;
    }

    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(?string $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    public function getPlainPassword(): string
    {
        return (string) $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }
    
    public function getOldPassword(): ?string
    {
        return $this->oldPassword;
    }

    public function setOldPassword(string $oldPassword): ?string
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    public function getCertified(): ?bool
    {
        return $this->certified;
    }

    public function setCertified(?bool $certified): self
    {
        $this->certified = $certified;

        return $this;
    }

    /**
     * @return Collection|SomeThing[]
     */
    public function getSomeThings(): Collection
    {
        return $this->someThings;
    }

    public function addSomeThing(SomeThing $someThing): self
    {
        if (!$this->someThings->contains($someThing)) {
            $this->someThings[] = $someThing;
            $someThing->setOwner($this);
        }

        return $this;
    }

    public function removeSomeThing(SomeThing $someThing): self
    {
        if ($this->someThings->contains($someThing)) {
            $this->someThings->removeElement($someThing);
            // set the owning side to null (unless already changed)
            if ($someThing->getOwner() === $this) {
                $someThing->setOwner(null);
            }
        }

        return $this;
    }
}
