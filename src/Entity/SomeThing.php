<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\SomeThingRepository;

/**
 * @ORM\Entity(repositoryClass=SomeThingRepository::class)
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")  
 * @ORM\DiscriminatorMap
    ({
        "MAISON" = "Maison", 
        "APPARTEMENT" = "Appartement", 
        "STUDIO" = "Studio",
        "CHAMBRE" = "Chambre",
        "IMMEUBLE" = "Immeuble",
        "TERRAIN" = "Terrain"
    })  
*/
abstract class SomeThing
{    
    use TraitBaseEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $operation;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default": 0})
     */
    protected $views;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="someThings")
     */
    private $owner;

    /**
     * @ORM\OneToOne(targetEntity=Price::class, inversedBy="someThing", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $price;

    /**
     * @ORM\OneToOne(targetEntity=Localisation::class, inversedBy="someThing", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $localisation;

    public function __construct()
    {
        $this->views = 0;
    }
    
    public function __toString(): ?string
    {
        return $this->name;
    }

    protected function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOperation(): ?string
    {
        return $this->operation;
    }

    public function setOperation(string $operation): self
    {
        $this->operation = $operation;

        return $this;
    }

    public function getViews(): ?string
    {
        return $this->views;
    }

    public function setViews(?string $views): self
    {
        $this->views = $views;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getPrice(): ?Price
    {
        return $this->price;
    }

    public function setPrice(Price $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getLocalisation(): ?Localisation
    {
        return $this->localisation;
    }

    public function setLocalisation(Localisation $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }
}
