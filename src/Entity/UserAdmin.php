<?php

namespace App\Entity;

use App\Repository\AdminUserRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\User;

/**
 * @ORM\Entity(repositoryClass=AdminUserRepository::class)
 */
class UserAdmin extends User
{
    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getType()
    {
        return 'ADMIN';
    }
}
