<?php

namespace App\Entity;

use App\Repository\Extra\ExtraImmeubleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExtraImmeubleRepository::class)
 */
class ExtraImmeuble
{
    use TraitExtra;
    use TraitBaseEntity;

    public function __construct()
    {
    }
}
