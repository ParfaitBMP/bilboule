<?php

namespace App\Entity;

use App\Repository\Ad\AnnonceStudioRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnnonceStudioRepository::class)
 */
class AnnonceStudio extends Annonce
{
    /**
     * @ORM\ManyToOne(targetEntity=Studio::class, inversedBy="annonces", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $studio;

    public function __construct()
    {
        parent::__construct();
    }

    public function __toString(): ?string
    {
        return parent::__toString();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getStudio(): ?Studio
    {
        return $this->studio;
    }

    public function setStudio(?Studio $studio): self
    {
        $this->studio = $studio;

        return $this;
    }

    public function getType()
    {
        return 'STUDIO';
    }

    public function getIndexRoute()
    {
        return 'annonce_studio_index';
    }

    public function getNewRoute()
    {
        return 'annonce_studio_new';
    }

    public function getShowRoute()
    {
        return 'annonce_studio_show';
    }

    public function getEditRoute()
    {
        return 'annonce_studio_edit';
    }
}
