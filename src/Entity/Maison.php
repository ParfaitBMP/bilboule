<?php

namespace App\Entity;

use App\Repository\Thing\MaisonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=MaisonRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class Maison extends Logement
{
    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $nbrEtage;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $nbrGarage;

    /**
     * @ORM\OneToMany(targetEntity=AnnonceMaison::class, mappedBy="maison", cascade={"remove"})
     */
    private $annonces;

    /**
     * @ORM\OneToMany(targetEntity=PjMaison::class, mappedBy="maison", cascade={"persist", "remove"})
     */
    private $piecesJointes;

    /**
     * @ORM\ManyToMany(targetEntity=ExtraLogement::class)
     */
    private $extraLogements;

    public function __construct()
    {
        parent::__construct();
        $this->annonces = new ArrayCollection();
        $this->piecesJointes = new ArrayCollection();
        $this->extraLogements = new ArrayCollection();
        $this->nbrEtage = 0;
        $this->nbrGarage = 0;
    }

    public function __toString(): ?string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getName(): ?string
    {
        return '[Maison] '. parent::getName();
    }

    public function getNbrEtage(): ?int
    {
        return $this->nbrEtage;
    }

    public function setNbrEtage(?int $nbrEtage): self
    {
        $this->nbrEtage = $nbrEtage;

        return $this;
    }

    public function getNbrGarage(): ?int
    {
        return $this->nbrGarage;
    }

    public function setNbrGarage(?int $nbrGarage): self
    {
        $this->nbrGarage = $nbrGarage;

        return $this;
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annonces[] = $annonce;
            $annonce->setMaison($this);
        }

        return $this;
    }

    public function removeAnnonce(Annonce $annonce): self
    {
        if ($this->annonces->contains($annonce)) {
            $this->annonces->removeElement($annonce);
            // set the owning side to null (unless already changed)
            if ($annonce->getMaison() === $this) {
                $annonce->setMaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PjMaison[]
     */
    public function getPiecesJointes(): Collection
    {
        return $this->piecesJointes;
    }

    public function addPiecesJointe(PjMaison $piecesJointe): self
    {
        if (!$this->piecesJointes->contains($piecesJointe)) {
            $this->piecesJointes[] = $piecesJointe;
            $piecesJointe->setMaison($this);
        }

        return $this;
    }

    public function removePiecesJointe(PjMaison $piecesJointe): self
    {
        if ($this->piecesJointes->contains($piecesJointe)) {
            $this->piecesJointes->removeElement($piecesJointe);
            // set the owning side to null (unless already changed)
            if ($piecesJointe->getMaison() === $this) {
                $piecesJointe->setMaison(null);
            }
        }

        return $this;
    }

    public function getDimension(): ?Dimension
    {
        return parent::getDimension();
    }

    public function setDimension(?Dimension $dimension): Property
    {
        parent::setDimension($dimension);

        return $this;
    }

    public function getIndexRoute()
    {
        return 'maison_index';
    }

    public function getNewRoute()
    {
        return 'maison_new';
    }

    public function getShowRoute()
    {
        return 'maison_show';
    }

    public function getEditRoute()
    {
        return 'maison_edit';
    }

    /**
     * @return Collection|ExtraLogement[]
     */
    public function getExtraLogements(): Collection
    {
        return $this->extraLogements;
    }

    public function addExtraLogement(ExtraLogement $extraLogement): self
    {
        if (!$this->extraLogements->contains($extraLogement)) {
            $this->extraLogements[] = $extraLogement;
        }

        return $this;
    }

    public function removeExtraLogement(ExtraLogement $extraLogement): self
    {
        if ($this->extraLogements->contains($extraLogement)) {
            $this->extraLogements->removeElement($extraLogement);
        }

        return $this;
    }
}
