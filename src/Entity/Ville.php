<?php

namespace App\Entity;

use App\Repository\Map\VilleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VilleRepository::class)
 */
class Ville extends Place
{
    /**
     * @ORM\OneToMany(targetEntity=Arrondissement::class, mappedBy="ville")
     */
    private $arrondissements;

    /**
     * @ORM\ManyToOne(targetEntity=Departement::class, inversedBy="villes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $departement;

    public function __construct()
    {
        $this->arrondissements = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return parent::getName();
    }

    public function getTree(): ?string
    {
        return $this->departement->getTree() .', '. $this->departement->getName();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }
    
    /**
     * @return Collection|Arrondissement[]
     */
    public function getArrondissements(): Collection
    {
        return $this->arrondissements;
    }

    public function addArrondissement(Arrondissement $arrondissement): self
    {
        if (!$this->arrondissements->contains($arrondissement)) {
            $this->arrondissements[] = $arrondissement;
            $arrondissement->setVille($this);
        }

        return $this;
    }

    public function removeArrondissement(Arrondissement $arrondissement): self
    {
        if ($this->arrondissements->contains($arrondissement)) {
            $this->arrondissements->removeElement($arrondissement);
            // set the owning side to null (unless already changed)
            if ($arrondissement->getVille() === $this) {
                $arrondissement->setVille(null);
            }
        }

        return $this;
    }

    public function getDepartement(): ?Departement
    {
        return $this->departement;
    }

    public function setDepartement(?Departement $departement): self
    {
        $this->departement = $departement;

        return $this;
    }
}
