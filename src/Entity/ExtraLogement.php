<?php

namespace App\Entity;

use App\Repository\Extra\ExtraLogementRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExtraLogementRepository::class)
 */
class ExtraLogement
{
    use TraitExtra;
    use TraitBaseEntity;

    public function __construct()
    {
    }

    public function __toString(): ?string
    {
        return $this->name;
    }
}
