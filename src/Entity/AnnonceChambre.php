<?php

namespace App\Entity;

use App\Repository\Ad\AnnonceChambreRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnnonceChambreRepository::class)
 */
class AnnonceChambre extends Annonce
{
    /**
     * @ORM\ManyToOne(targetEntity=Chambre::class, inversedBy="annonces", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $chambre;

    public function __construct()
    {
        parent::__construct();
    }

    public function __toString(): ?string
    {
        return parent::__toString();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getChambre(): ?Chambre
    {
        return $this->chambre;
    }

    public function setChambre(?Chambre $chambre): self
    {
        $this->chambre = $chambre;

        return $this;
    }

    public function getType()
    {
        return 'CHAMBRE';
    }

    public function getIndexRoute()
    {
        return 'annonce_chambre_index';
    }

    public function getNewRoute()
    {
        return 'annonce_chambre_new';
    }

    public function getShowRoute()
    {
        return 'annonce_chambre_show';
    }

    public function getEditRoute()
    {
        return 'annonce_chambre_edit';
    }
}
