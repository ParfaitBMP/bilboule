<?php

namespace App\Entity;

use App\Repository\Map\LocaliteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LocaliteRepository::class)
 */
class Localite extends Place
{
    /**
     * @ORM\ManyToOne(targetEntity=Arrondissement::class, inversedBy="localites")
     * @ORM\JoinColumn(nullable=true)
     */
    private $arrondissement;

    public function __toString(): ?string
    {
        return parent::getName();
    }

    public function getTree(): ?string
    {
        return $this->arrondissement->getTree() .', '. $this->arrondissement->getName();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getArrondissement(): ?Arrondissement
    {
        return $this->arrondissement;
    }

    public function setArrondissement(?Arrondissement $arrondissement): self
    {
        $this->arrondissement = $arrondissement;

        return $this;
    }
}
