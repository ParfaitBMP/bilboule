<?php

namespace App\Entity;

use App\Repository\Thing\TerrainRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=TerrainRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class Terrain extends Property
{
    /**
     * @ORM\Column(type="float")
     */
    private $largeur;

    /**
     * @ORM\Column(type="float")
     */
    private $longueur;

    /**
     * @ORM\Column(type="float")
     */
    private $perimetre;

    /**
     * @ORM\Column(type="float")
     */
    private $superficie;

    /**
     * @ORM\OneToMany(targetEntity=AnnonceTerrain::class, mappedBy="terrain")
     */
    private $annonces;

    /**
     * @ORM\ManyToMany(targetEntity=ExtraTerrain::class)
     */
    private $extraTerrains;

    /**
     * @ORM\OneToMany(targetEntity=PjTerrain::class, mappedBy="terrain", cascade={"persist", "remove"})
     */
    private $piecesJointes;

    public function __construct()
    {
        parent::__construct();
        $this->annonces = new ArrayCollection();
        $this->extraTerrains = new ArrayCollection();
        $this->piecesJointes = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return parent::__toString();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getLargeur(): ?float
    {
        return $this->largeur;
    }

    public function setLargeur(float $largeur): self
    {
        $this->largeur = $largeur;

        return $this;
    }

    public function getLongueur(): ?float
    {
        return $this->longueur;
    }

    public function setLongueur(float $longueur): self
    {
        $this->longueur = $longueur;

        return $this;
    }

    public function getPerimetre(): ?float
    {
        return $this->perimetre;
    }

    public function setPerimetre(float $perimetre): self
    {
        $this->perimetre = $perimetre;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function computePerimetre(): float
    {
        $this->perimetre = $this->largeur + $this->longueur;

        return $this->perimetre;
    }

    public function getSuperficie(): ?float
    {
        return $this->superficie;
    }

    public function setSuperficie(float $superficie): self
    {
        $this->superficie = $superficie;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function computeSuperficie(): float
    {
        $this->superficie = $this->largeur * $this->longueur;

        return $this->superficie;
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annonces[] = $annonce;
            $annonce->setTerrain($this);
        }

        return $this;
    }

    public function removeAnnonce(Annonce $annonce): self
    {
        if ($this->annonces->contains($annonce)) {
            $this->annonces->removeElement($annonce);
            // set the owning side to null (unless already changed)
            if ($annonce->getTerrain() === $this) {
                $annonce->setTerrain(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ExtraTerrain[]
     */
    public function getExtraTerrains(): Collection
    {
        return $this->extraTerrains;
    }

    public function addExtraTerrain(ExtraTerrain $extraTerrain): self
    {
        if (!$this->extraTerrains->contains($extraTerrain)) {
            $this->extraTerrains[] = $extraTerrain;
        }

        return $this;
    }

    public function removeExtraTerrain(ExtraTerrain $extraTerrain): self
    {
        if ($this->extraTerrains->contains($extraTerrain)) {
            $this->extraTerrains->removeElement($extraTerrain);
        }

        return $this;
    }

    /**
     * @return Collection|PjTerrain[]
     */
    public function getPiecesJointes(): Collection
    {
        return $this->piecesJointes;
    }

    public function addPiecesJointe(PjTerrain $piecesJointe): self
    {
        if (!$this->piecesJointes->contains($piecesJointe)) {
            $this->piecesJointes[] = $piecesJointe;
            $piecesJointe->setTerrain($this);
        }

        return $this;
    }

    public function removePiecesJointe(PjTerrain $piecesJointe): self
    {
        if ($this->piecesJointes->contains($piecesJointe)) {
            $this->piecesJointes->removeElement($piecesJointe);
            // set the owning side to null (unless already changed)
            if ($piecesJointe->getTerrain() === $this) {
                $piecesJointe->setTerrain(null);
            }
        }

        return $this;
    }

    public function getIndexRoute()
    {
        return 'terrain_index';
    }

    public function getNewRoute()
    {
        return 'terrain_new';
    }

    public function getShowRoute()
    {
        return 'terrain_show';
    }

    public function getEditRoute()
    {
        return 'terrain_edit';
    }
}
