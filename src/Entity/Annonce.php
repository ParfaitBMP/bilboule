<?php

namespace App\Entity;

use App\Repository\Ad\AnnonceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=AnnonceRepository::class)
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")  
 * @ORM\DiscriminatorMap
    ({
        "MAISON" = "AnnonceMaison", 
        "APPARTEMENT" = "AnnonceAppartement", 
        "STUDIO" = "AnnonceStudio",
        "CHAMBRE" = "AnnonceChambre",
        "IMMEUBLE" = "AnnonceImmeuble",
        "TERRAIN" = "AnnonceTerrain"
    })  
 */
abstract class Annonce
{
    use TraitBaseEntity;
    
    public const STATE_DRAFT = 'DRAFT';
    public const STATE_PENDING = 'PENDING';
    public const STATE_DONE = 'DONE';
    public const STATE_CANCEL = 'CANCEL';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publishDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expireDate;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="annonces")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=Formule::class, inversedBy="annonces")
     * @ORM\JoinColumn(nullable=true)
     */
    private $formule;

    /**
     * @ORM\Column(type="integer")
     */
    private $duration;

    /**
     * @ORM\Column(type="float", options={"default": 100})
     */
    private $amountDay;

    public function __construct()
    {
        $this->state = self::STATE_DRAFT;
        $this->duration = 5;
    }

    public function __toString(): ?string
    {
        return $this->name;
    }

    protected function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getPublishDate(): ?\DateTimeInterface
    {
        return $this->publishDate;
    }

    public function setPublishDate(?\DateTimeInterface $publishDate): self
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    public function getExpireDate(): ?\DateTimeInterface
    {
        return $this->expireDate;
    }

    public function setExpireDate(?\DateTimeInterface $expireDate): self
    {
        $this->expireDate = $expireDate;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getFormule(): ?Formule
    {
        return $this->formule;
    }

    public function setFormule(?Formule $formule): self
    {
        $this->formule = $formule;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function setToDone() :self
    {
        $this->state = self::STATE_DONE;
        $this->publishDate = new \DateTime();
        $this->expireDate = (new \DateTime())->add(new \DateInterval('P'.$this->duration.'D')); 

        return $this;
    }

    public function setToPending(): self
    {
        $this->state = self::STATE_PENDING;
        $this->resetPublish();

        return $this;
    }

    public function setToDraft(): self
    {
        $this->state = self::STATE_DRAFT;
        $this->resetPublish();

        return $this;
    }

    public function setToCancel(): self
    {
        $this->state = self::STATE_CANCEL;
        $this->resetPublish();

        return $this;
    }

    public function isDraft()
    {
        return $this->state == self::STATE_DRAFT;
    }

    public function isPending()
    {
        return $this->state == self::STATE_PENDING;
    }

    public function isPublished()
    {
        return $this->state == self::STATE_DONE;
    }

    public function isCanceled()
    {
        return $this->state == self::STATE_CANCEL;
    }

    public function resetPublish()
    {
        $this->publishDate = null;
        $this->expireDate = null; 

        return $this;
    }


    public function getType()
    {
        return 'ANNONCE';
    }

    public function getAmountDay(): ?float
    {
        return $this->amountDay;
    }

    public function setAmountDay(float $amountDay): self
    {
        $this->amountDay = $amountDay;

        return $this;
    }
}
