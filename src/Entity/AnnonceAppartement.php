<?php

namespace App\Entity;

use App\Repository\Ad\AnnonceAppartementRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnnonceAppartementRepository::class)
 */
class AnnonceAppartement extends Annonce
{
    /**
     * @ORM\ManyToOne(targetEntity=Appartement::class, inversedBy="annonces", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $appartement;

    public function __construct()
    {
        parent::__construct();
    }

    public function __toString(): ?string
    {
        return parent::__toString();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getAppartement(): ?Appartement
    {
        return $this->appartement;
    }

    public function setAppartement(?Appartement $appartement): self
    {
        $this->appartement = $appartement;

        return $this;
    }

    public function getType()
    {
        return 'APPARTEMENT';
    }

    public function getIndexRoute()
    {
        return 'annonce_appartement_index';
    }

    public function getNewRoute()
    {
        return 'annonce_appartement_new';
    }

    public function getShowRoute()
    {
        return 'annonce_appartement_show';
    }

    public function getEditRoute()
    {
        return 'annonce_appartement_edit';
    }
}
