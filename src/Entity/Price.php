<?php

namespace App\Entity;

use App\Repository\PriceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PriceRepository::class)
 */
class Price
{
    use TraitBaseEntity;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\OneToOne(targetEntity=SomeThing::class, mappedBy="price", cascade={"persist", "remove"})
     */
    private $someThing;

    public function __toString(): ?string
    {
        return $this->amount;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getSomeThing(): ?SomeThing
    {
        return $this->someThing;
    }

    public function setSomeThing(SomeThing $someThing): self
    {
        $this->someThing = $someThing;

        // set the owning side of the relation if necessary
        if ($someThing->getPrice() !== $this) {
            $someThing->setPrice($this);
        }

        return $this;
    }
}
