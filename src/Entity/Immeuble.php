<?php

namespace App\Entity;

use App\Repository\Thing\ImmeubleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ImmeubleRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class Immeuble extends Property
{
    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $levelNumber;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $parkingNumber;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $elevatorNumber;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $garageNumber;

    /**
     * @ORM\OneToMany(targetEntity=AnnonceImmeuble::class, mappedBy="immeuble")
     */
    protected $annonces;

    /**
     * @ORM\ManyToMany(targetEntity=ExtraImmeuble::class)
     */
    protected $extraImmeubles;

    /**
     * @ORM\OneToMany(targetEntity=PjImmeuble::class, mappedBy="immeuble", cascade={"persist", "remove"})
     */
    private $piecesJointes;

    public function __construct()
    {
        parent::__construct();
        $this->annonces = new ArrayCollection();
        $this->extraImmeubles = new ArrayCollection();
        $this->piecesJointes = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return parent::__toString();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getLevelNumber(): ?int
    {
        return $this->levelNumber;
    }

    public function setLevelNumber(?int $levelNumber): self
    {
        $this->levelNumber = $levelNumber;

        return $this;
    }

    public function getParkingNumber(): ?int
    {
        return $this->parkingNumber;
    }

    public function setParkingNumber(?int $parkingNumber): self
    {
        $this->parkingNumber = $parkingNumber;

        return $this;
    }

    public function getElevatorNumber(): ?int
    {
        return $this->elevatorNumber;
    }

    public function setElevatorNumber(?int $elevatorNumber): self
    {
        $this->elevatorNumber = $elevatorNumber;

        return $this;
    }

    public function getGarageNumber(): ?int
    {
        return $this->garageNumber;
    }

    public function setGarageNumber(?int $garageNumber): self
    {
        $this->garageNumber = $garageNumber;

        return $this;
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annonces[] = $annonce;
            $annonce->setImmeuble($this);
        }

        return $this;
    }

    public function removeAnnonce(Annonce $annonce): self
    {
        if ($this->annonces->contains($annonce)) {
            $this->annonces->removeElement($annonce);
            // set the owning side to null (unless already changed)
            if ($annonce->getImmeuble() === $this) {
                $annonce->setImmeuble(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ExtraImmeuble[]
     */
    public function getExtraImmeubles(): Collection
    {
        return $this->extraImmeubles;
    }

    public function addExtraImmeuble(ExtraImmeuble $extraImmeuble): self
    {
        if (!$this->extraImmeubles->contains($extraImmeuble)) {
            $this->extraImmeubles[] = $extraImmeuble;
        }

        return $this;
    }

    public function removeExtraImmeuble(ExtraImmeuble $extraImmeuble): self
    {
        if ($this->extraImmeubles->contains($extraImmeuble)) {
            $this->extraImmeubles->removeElement($extraImmeuble);
        }

        return $this;
    }

    /**
     * @return Collection|PjImmeuble[]
     */
    public function getPiecesJointes(): Collection
    {
        return $this->piecesJointes;
    }

    public function addPiecesJointe(PjImmeuble $piecesJointe): self
    {
        if (!$this->piecesJointes->contains($piecesJointe)) {
            $this->piecesJointes[] = $piecesJointe;
            $piecesJointe->setImmeuble($this);
        }

        return $this;
    }

    public function removePiecesJointe(PjImmeuble $piecesJointe): self
    {
        if ($this->piecesJointes->contains($piecesJointe)) {
            $this->piecesJointes->removeElement($piecesJointe);
            // set the owning side to null (unless already changed)
            if ($piecesJointe->getImmeuble() === $this) {
                $piecesJointe->setImmeuble(null);
            }
        }

        return $this;
    }

    public function getIndexRoute()
    {
        return 'immeuble_index';
    }

    public function getNewRoute()
    {
        return 'immeuble_new';
    }

    public function getShowRoute()
    {
        return 'immeuble_show';
    }

    public function getEditRoute()
    {
        return 'immeuble_edit';
    }
}
