<?php

namespace App\Entity;

use App\Repository\Thing\ChambreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ChambreRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class Chambre extends Logement
{
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $estModerne;

    /**
     * @ORM\OneToMany(targetEntity=AnnonceChambre::class, mappedBy="chambre")
     */
    private $annonces;

    /**
     * @ORM\OneToMany(targetEntity=PjChambre::class, mappedBy="chambre", cascade={"persist", "remove"})
     */
    private $piecesJointes;

    /**
     * @ORM\ManyToMany(targetEntity=ExtraLogement::class)
     */
    private $extraLogements;


    public function __construct()
    {
        parent::__construct();
        $this->annonces = new ArrayCollection();
        $this->piecesJointes = new ArrayCollection();
        $this->extraLogements = new ArrayCollection();
        
        //Valeurs par défaut
        $this->nbrSalon = 0;
        $this->nbrChambre = 1;
        $this->nbrCuisine = 0;
        $this->nbrSalleDeBain = 0;
    }

    public function __toString(): ?string
    {
        return parent::__toString();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getEstModerne(): ?bool
    {
        return $this->estModerne;
    }

    public function setEstModerne(?bool $estModerne): self
    {
        $this->estModerne = $estModerne;

        return $this;
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annonces[] = $annonce;
            $annonce->setChambre($this);
        }

        return $this;
    }

    public function removeAnnonce(Annonce $annonce): self
    {
        if ($this->annonces->contains($annonce)) {
            $this->annonces->removeElement($annonce);
            // set the owning side to null (unless already changed)
            if ($annonce->getChambre() === $this) {
                $annonce->setChambre(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PjChambre[]
     */
    public function getPiecesJointes(): Collection
    {
        return $this->piecesJointes;
    }

    public function addPiecesJointe(PjChambre $piecesJointe): self
    {
        if (!$this->piecesJointes->contains($piecesJointe)) {
            $this->piecesJointes[] = $piecesJointe;
            $piecesJointe->setChambre($this);
        }

        return $this;
    }

    public function removePiecesJointe(PjChambre $piecesJointe): self
    {
        if ($this->piecesJointes->contains($piecesJointe)) {
            $this->piecesJointes->removeElement($piecesJointe);
            // set the owning side to null (unless already changed)
            if ($piecesJointe->getChambre() === $this) {
                $piecesJointe->setChambre(null);
            }
        }

        return $this;
    }

    public function getIndexRoute()
    {
        return 'chambre_index';
    }

    public function getNewRoute()
    {
        return 'chambre_new';
    }

    public function getShowRoute()
    {
        return 'chambre_show';
    }

    public function getEditRoute()
    {
        return 'chambre_edit';
    }

    /**
     * @return Collection|ExtraLogement[]
     */
    public function getExtraLogements(): Collection
    {
        return $this->extraLogements;
    }

    public function addExtraLogement(ExtraLogement $extraLogement): self
    {
        if (!$this->extraLogements->contains($extraLogement)) {
            $this->extraLogements[] = $extraLogement;
        }

        return $this;
    }

    public function removeExtraLogement(ExtraLogement $extraLogement): self
    {
        if ($this->extraLogements->contains($extraLogement)) {
            $this->extraLogements->removeElement($extraLogement);
        }

        return $this;
    }
}