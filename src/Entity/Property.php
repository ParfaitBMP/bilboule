<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use App\Repository\Map\PropertyRepository;

/**
 * @ORM\Entity(repositoryClass=PropertyRepository::class)
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")  
 * @ORM\DiscriminatorMap
    ({
        "LOGEMENT" = "Logement",
        "IMMEUBLE" = "Immeuble",
        "TERRAIN" = "Terrain"
    })
 */
abstract class Property extends SomeThing
{
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $dateDisponibilite;

    /**
     * @ORM\OneToOne(targetEntity=Dimension::class, inversedBy="property", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $dimension;

    public function __construct()
    {
        $this->dimension = null;
    }
    
    public function __toString(): ?string
    {
        return parent::__toString();
    }

    protected function getId(): ?int
    {
        return parent::getId();
    }

    public function getDateDisponibilite(): ?\DateTimeInterface
    {
        return $this->dateDisponibilite;
    }

    public function setDateDisponibilite(?\DateTimeInterface $dateDisponibilite): self
    {
        $this->dateDisponibilite = $dateDisponibilite;

        return $this;
    }

    public function getDimension(): ?Dimension
    {
        return $this->dimension;
    }

    public function setDimension(?Dimension $dimension): self
    {
        $this->dimension = $dimension;

        return $this;
    }
}
