<?php

namespace App\Entity;

use App\Repository\Map\ArrondissementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArrondissementRepository::class)
 */
class Arrondissement extends Place
{
    /**
     * @ORM\ManyToOne(targetEntity=Ville::class, inversedBy="arrondissements")
     * @ORM\JoinColumn(nullable=true)
     */
    private $ville;

    /**
     * @ORM\OneToMany(targetEntity=Localite::class, mappedBy="arrondissement")
     */
    private $localites;

    public function __construct()
    {
        $this->localites = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return parent::getName();
    }

    public function getTree(): ?string
    {
        return $this->ville->getTree() .', '. $this->ville->getName();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return Collection|Localite[]
     */
    public function getLocalites(): Collection
    {
        return $this->localites;
    }

    public function addLocalite(Localite $localite): self
    {
        if (!$this->localites->contains($localite)) {
            $this->localites[] = $localite;
            $localite->setArrondissement($this);
        }

        return $this;
    }

    public function removeLocalite(Localite $localite): self
    {
        if ($this->localites->contains($localite)) {
            $this->localites->removeElement($localite);
            // set the owning side to null (unless already changed)
            if ($localite->getArrondissement() === $this) {
                $localite->setArrondissement(null);
            }
        }

        return $this;
    }
}
