<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use App\Repository\Map\LogementRepository;

/**
 * @ORM\Entity(repositoryClass=LogementRepository::class)
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")  
 * @ORM\DiscriminatorMap
    ({
        "MAISON" = "Maison", 
        "APPARTEMENT" = "Appartement", 
        "STUDIO" = "Studio",
        "CHAMBRE" = "Chambre"
    })
 */
abstract class Logement extends Property
{
    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $nbrSalon;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $nbrChambre;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $nbrCuisine;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $nbrSalleDeBain;

    public function __construct()
    {
        $this->nbrSalon = 1;
        $this->nbrChambre = 1;
        $this->nbrCuisine = 1;
        $this->nbrSalleDeBain = 1;
    }

    public function __toString():? string
    {
        return parent::__toString();
    }

    protected function getId(): ?int
    {
        return parent::getId();
    }

    public function getNbrSalon(): ?int
    {
        return $this->nbrSalon;
    }

    public function setNbrSalon(?int $nbrSalon): self
    {
        $this->nbrSalon = $nbrSalon;

        return $this;
    }

    public function getNbrChambre(): ?int
    {
        return $this->nbrChambre;
    }

    public function setNbrChambre(?int $nbrChambre): self
    {
        $this->nbrChambre = $nbrChambre;

        return $this;
    }

    public function getNbrCuisine(): ?int
    {
        return $this->nbrCuisine;
    }

    public function setNbrCuisine(?int $nbrCuisine): self
    {
        $this->nbrCuisine = $nbrCuisine;

        return $this;
    }

    public function getNbrSalleDeBain(): ?int
    {
        return $this->nbrSalleDeBain;
    }

    public function setNbrSalleDeBain(?int $nbrSalleDeBain): self
    {
        $this->nbrSalleDeBain = $nbrSalleDeBain;

        return $this;
    }

    public function getDimension(): ?Dimension
    {
        return parent::getDimension();
    }

    public function setDimension(?Dimension $dimension): Property
    {
        parent::setDimension($dimension);

        return $this;
    }
}
