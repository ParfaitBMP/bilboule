<?php

namespace App\Entity;

use App\Repository\Extra\ExtraTerrainRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExtraTerrainRepository::class)
 */
class ExtraTerrain
{
    use TraitExtra;
    use TraitBaseEntity;

    public function __construct()
    {
    }
}
