<?php

namespace App\Entity;

use App\Repository\Ad\AnnonceTerrainRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnnonceTerrainRepository::class)
 */
class AnnonceTerrain extends Annonce
{
    /**
     * @ORM\ManyToOne(targetEntity=Terrain::class, inversedBy="annonces", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $terrain;

    public function __construct()
    {
        parent::__construct();
    }

    public function __toString(): ?string
    {
        return parent::__toString();
    }

    public function getId(): ?int
    {
        return parent::getId();
    }

    public function getTerrain(): ?Terrain
    {
        return $this->terrain;
    }

    public function setTerrain(?Terrain $terrain): self
    {
        $this->terrain = $terrain;

        return $this;
    }

    public function getType()
    {
        return 'TERRAIN';
    }

    public function getIndexRoute()
    {
        return 'annonce_terrain_index';
    }

    public function getNewRoute()
    {
        return 'annonce_terrain_new';
    }

    public function getShowRoute()
    {
        return 'annonce_terrain_show';
    }

    public function getEditRoute()
    {
        return 'annonce_terrain_edit';
    }
}
