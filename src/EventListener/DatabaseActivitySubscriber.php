<?php

namespace App\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Entity\User;


class DatabaseActivitySubscriber implements EventSubscriber
{
    private $tokenStorage;
    private $slugger;

    public function __construct(TokenStorageInterface $tokenStorage, SluggerInterface $slugger)
    {
        $this->tokenStorage = $tokenStorage;
        $this->slugger = $slugger;
    }

    // this method can only return the event names; you cannot define a
    // custom method name to execute when each event triggers
    public function getSubscribedEvents()
    {
        return [
            // Events::prePersist,
            // Events::preUpdate,
        ];
    }

    // callback methods must be called exactly like the events they listen to;
    // they receive an argument of type LifecycleEventArgs, which gives you access
    // to both the entity object of the event and the entity manager itself
    // public function prePersist(LifecycleEventArgs $args)
    // {
    // 	$entity = $args->getObject();


    //     if($this->checkClassMethods(['getName', 'getSlug', 'setCreateDate', 'setCreateUser'], $entity))
    //     {
    //         //MAJ du slug
    //         $entity->setSlug($this->slugger->slug($entity->getName()));

    //     	if(!$entity->getCreateDate() && !$entity->getCreateUser())
    //     	{
    //             $user = ($this->tokenStorage->getToken()->getUser() instanceof User)? $this->tokenStorage->getToken()->getUser() : null;
    // 	        $entity->setCreateUser($user);
    // 	        $entity->setCreateDate(new \Datetime());
    // 	    }
    //     }
    // }

    // public function preUpdate(LifecycleEventArgs $args)
    // {
    // 	$entity = $args->getObject();

    // 	if(property_exists($entity, 'name') && property_exists($entity, 'slug')){
	   //      $entity->setSlug($this->slugger->slug($entity->getName()));
    // 	}

    //     if(property_exists($entity, 'writeUser') && property_exists($entity, 'writeDate')){
    //         $user = ($this->tokenStorage->getToken()->getUser() instanceof User)? $this->tokenStorage->getToken()->getUser() : null;
    //         $entity->setWriteUser($user);
    //         $entity->setWriteDate(new \Datetime());
    //     }
    // }

    // public function checkClassMethods($methodsTab, $object){
    //     $reflect = new \ReflectionClass($object);
    //     $classMethods = $reflect->getMethods();
    //     // $properties = $reflect->getProperties();

    //     $globalTest = false;
    //     $test = true;

    //     foreach ($methodsTab as $methodName) {
    //         $testMethod = true;

    //         foreach ($classMethods as $classMethod) {
    //             if($classMethod->name == $methodName)
    //                 $testMethod = true;
    //             else
    //                 $testMethod = false;

    //             $test = $test and $testMethod;
    //             continue;
    //         }

    //     }

    //     die;

    //     return $test;
    // }
}