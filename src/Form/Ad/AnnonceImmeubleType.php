<?php

namespace App\Form\Ad;

use App\Entity\AnnonceImmeuble;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnnonceImmeubleType extends AnnonceType
{
    public function getParent() {
        return AnnonceType::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AnnonceImmeuble::class,
        ]);
    }
}
