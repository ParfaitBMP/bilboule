<?php

namespace App\Form\Ad;

use App\Entity\AnnonceMaison;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnnonceMaisonType extends AnnonceType
{
    public function getParent() {
        return AnnonceType::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AnnonceMaison::class,
        ]);
    }
}
