<?php

namespace App\Form\Ad;

use App\Entity\Annonce;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class AnnonceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Titre',
                'help' => 'Il s\'agit du libellé de votre annonce qui sera affiché aux visiteurs'
            ])
            ->add('duration', null, [
                'label' => 'Durée d\'affichage',
                'help' => 'Nombre de jours pendant lesquels votre annonce sera diffusée sur la plateforme',
                'attr' => ['placeholder' => 'Durée annonce', 'min' => 5]
            ])
            ->add('amountDay', MoneyType::class, [
                'label' => 'Montant journalier',
                'currency' => 'XAF', 'scale' => 50,
                'help' => 'Coût de l\'annonce par jour',
                'attr' => ['placeholder' => 'Montant/jour', 'min' => 100]
            ])
            ->add('formule', null, [
                'help' => 'Il s\'agit des avantages liés à votre annonce',
                'expanded' => true,
                'by_reference' => ($options['TYPE_FORM'] == 'EDIT')? true : false // Pour forcer le l'appel de setFormule() et donc MAJ auto du champ duration
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'TYPE_FORM' => 'NEW'
        ]);
    }
}
