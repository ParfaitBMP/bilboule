<?php

namespace App\Form;

use App\Entity\Dimension;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class DimensionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('availableArea', NumberType::class, [
                'label' => 'Espace disponible (en m2)',
                'help' => 'Il s\'agit de l\'espace disponible sur la superficie totale.',
                'attr' => ['min' => 0]
            ])
            ->add('livingArea', NumberType::class, [
                'label' => 'Espace occupable (en m2)',
                'help' => 'L\'espace occupable est relatif à l\'espace disponible). Autrement dit, quel espace est occupable sur l\'ensemble disponible?',
                'attr' => ['min' => 0]
            ])
            ->add('totalArea', NumberType::class, [
                'label' => 'Espace total (en m2)',
                'help' => 'Espace total, incluant espace occupable et non occupable',
                'attr' => ['min' => 0]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dimension::class,
        ]);
    }
}
