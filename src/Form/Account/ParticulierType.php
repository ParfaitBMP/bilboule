<?php

namespace App\Form\Account;

use App\Entity\Particulier;
use App\Entity\Place;
use App\Repository\Map\PlaceRepository;
use App\Entity\SecteurActivite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ParticulierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prenom', null, [
                'attr' => ['required' => true]
            ])
            ->add('nom')
            ->add('sexe', ChoiceType::class, 
                array('choices' => 
                    array(
                        '' => '',
                        'Homme' => 'M',
                        'Femme' => 'F'
                    ),
                'required' => false
            ))
            ->add('secteurActivites', EntityType::class, [
                'class' => SecteurActivite::class,
                'multiple' => true,
                'choice_label' => 'name',
                'placeholder' => '',
                'label' => 'Secteur(s) d\'activité(s)',
                'required' => false,
            ])
            ->add('email')
            ->add('phone')
            ->add('place', EntityType::class, [
                'class' => Place::class,
                'choice_label' => 'name',
                'required' => false,
                // 'query_builder' => function (PlaceRepository $er) {
                //     return $er->createQueryBuilder('p')
                //                             ->setMaxResults(5);
                // },
            ])
            ->add('presentation')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Particulier::class,
            'place' => array()
        ]);
    }
}
