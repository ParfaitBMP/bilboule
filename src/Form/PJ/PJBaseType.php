<?php

namespace App\Form\PJ;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;

class PJBaseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Titre de la photo', 
                'help' => 'Décrivez la photo. Exemple, pour une maison: "Photo de la salle à manger..."',
                'attr' => [
                    'class' => 'form-control',
                    'required' => true
                ]
            ])
            ->add('pjFile', VichFileType::class, 
                array(
                    'label' => 'Photo',
                    'required' => false,
                    'by_reference' => false,
                    'allow_delete' => true,
                )
            )
        ;
    }
}
