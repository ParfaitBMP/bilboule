<?php

namespace App\Form\PJ;

use App\Entity\Pjappartement;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PjAppartementType extends PJBaseType
{
    public function getParent() {
        return PJBaseType::class;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pjappartement::class,
        ]);
    }
}
