<?php

namespace App\Form\PJ;

use App\Entity\PjTerrain;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PjTerrainType extends PJBaseType
{
    public function getParent() {
        return PJBaseType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PjTerrain::class,
        ]);
    }
}
