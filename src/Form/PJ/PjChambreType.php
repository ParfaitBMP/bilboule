<?php

namespace App\Form\PJ;

use App\Entity\PjChambre;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PjChambreType extends PJBaseType
{
    public function getParent() {
        return PJBaseType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PjChambre::class,
        ]);
    }
}
