<?php

namespace App\Form\PJ;

use App\Entity\PjStudio;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PjStudioType extends PJBaseType
{
    public function getParent() {
        return PJBaseType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PjStudio::class,
        ]);
    }
}
