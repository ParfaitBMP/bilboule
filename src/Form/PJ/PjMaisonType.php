<?php

namespace App\Form\PJ;

use App\Entity\PjMaison;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PjMaisonType extends PJBaseType
{
    public function getParent() {
        return PJBaseType::class;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PjMaison::class,
        ]);
    }
}
