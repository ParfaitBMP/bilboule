<?php

namespace App\Form\PJ;

use App\Entity\PjImmeuble;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PjImmeubleType extends PJBaseType
{
    public function getParent() {
        return PJBaseType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PjImmeuble::class,
        ]);
    }
}
