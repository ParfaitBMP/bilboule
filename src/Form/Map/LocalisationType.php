<?php

namespace App\Form\Map;

use App\Entity\Localisation;
use App\Entity\Place;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class LocalisationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('place', EntityType::class, [
                'class' => Place::class,
                'label' => 'Lieu',
                'help' => 'Choisissez une ville, un quartier, etc.'
            ])
            ->add('comment', null, [
                'label' => 'Description de l\'emplacement',
                'help' => 'Décrivez de manière précise l\'emplacement'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Localisation::class,
        ]);
    }
}
