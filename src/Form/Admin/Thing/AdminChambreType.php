<?php

namespace App\Form\Admin\Thing;

use App\Entity\Chambre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\PriceType;
use App\Form\DimensionType;
use App\Form\Map\LocalisationType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Form\Thing\ChambreType;


class AdminChambreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('owner', null, ['label' => 'Compte Propriétaire']);
    }

    public function getParent() {
        return ChambreType::class;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Chambre::class,
        ]);
    }
}
