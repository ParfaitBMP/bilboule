<?php

namespace App\Form\Thing;

use App\Entity\Maison;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\PJ\PjMaisonType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class MaisonType extends AbstractType
{
    public function getParent() {
        return LogementType::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nbrEtage', null, [
                'label' => 'Nombre étage', 
                'attr' => ['placeholder' => 'Nombre d\'étage', 'min' => 0]
            ])
            ->add('nbrGarage', null, [
                'label' => 'Nombre garage', 
                'attr' => ['placeholder' => 'Nombre de garage', 'min' => 0]
            ])
            ->add('piecesJointes', CollectionType::class, [
                'entry_type' => PjMaisonType::class,
                'entry_options' => ['label' => false, 'required' => true],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Maison::class,
        ]);
    }
}
