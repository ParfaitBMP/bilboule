<?php 

namespace App\Form\Thing;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Form\DimensionType;

class ProprieteType extends ThingType
{
	public function getParent() {
        return ThingType::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDisponibilite', null, [
                'label' => 'Date de disponibilité', 
                'attr' => ['placeholder' => 'Date de disponibilité'],
                'help' => 'Date à partir de laquelle votre propriété pourra être occupée.'
            ])
            ->add('dimension', DimensionType::class, [
                'attr' => ['required' => false]
            ])
        ;
    }
}