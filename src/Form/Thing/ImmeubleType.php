<?php

namespace App\Form\Thing;

use App\Entity\Immeuble;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\PJ\PjImmeubleType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ImmeubleType extends AbstractType
{
    public function getParent() {
        return ProprieteType::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('levelNumber', null, [
                'label' => 'Nombre étage', 
                'attr' => ['placeholder' => 'Nombre d\'étage', 'min' => 1],
                'help' => 'Nombre étage'
            ])
            ->add('elevatorNumber', null, [
                'label' => 'Nombre ascenseur', 
                'attr' => ['placeholder' => 'Nombre d\'ascenseur', 'min' => 0],
                'help' => 'Nombre ascenseur'
            ])
            ->add('parkingNumber', null, [
                'label' => 'Nombre parking', 
                'attr' => ['placeholder' => 'Nombre de parking', 'min' => 0],
                'help' => 'Nombre parking'
            ])
            ->add('garageNumber', null, [
                'label' => 'Nombre garage', 
                'attr' => ['placeholder' => 'Nombre de garage', 'min' => 0],
                'help' => 'Nombre garage'
            ])
            ->add('piecesJointes', CollectionType::class, [
                'entry_type' => PjImmeubleType::class,
                'entry_options' => ['label' => false, 'required' => true],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
            ->add('extraImmeubles', null, [
                'label' => 'Extras-Immeuble',
                'help' => 'Autres avatages disponibles pour votre propriété',
                'attr' => ['class' => 'ms']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Immeuble::class,
        ]);
    }
}
