<?php

namespace App\Form\Thing;

use App\Entity\Terrain;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\PJ\PjTerrainType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class TerrainType extends AbstractType
{
    public function getParent() {
        return ProprieteType::class;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('largeur', null, [
                'label' => 'Largeur (en mètre)', 
                'attr' => ['placeholder' => 'Largeur', 'min' => 1],
                'help' => 'Largeur du terrain (en mètre)'
            ])
            ->add('longueur', null, [
                'label' => 'Longueur (en mètre)', 
                'attr' => ['placeholder' => 'Longueur', 'min' => 1],
                'help' => 'Longueur du terrain (en mètre)'
            ])
            ->add('piecesJointes', CollectionType::class, [
                'entry_type' => PjTerrainType::class,
                'entry_options' => ['label' => false, 'required' => true],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
            ->add('extraTerrains', null, [
                'label' => 'Extras-Terrain',
                'help' => 'Autres avatages disponibles pour votre propriété',
                'attr' => ['class' => 'ms']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Terrain::class,
        ]);
    }
}