<?php 

namespace App\Form\Thing;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LogementType extends ProprieteType
{
	public function getParent() {
        return ProprieteType::class;
    }
	
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nbrSalon', null, [
                'label' => 'Nombre salon', 
                'attr' => ['placeholder' => 'Nombre de salon', 'min' => 0]
            ])
            ->add('nbrChambre', null, [
                'label' => 'Nombre chambre', 
                'attr' => ['placeholder' => 'Nombre de chambre', 'min' => 0]
            ])
            ->add('nbrCuisine', null, [
                'label' => 'Nombre cuisine', 
                'attr' => ['placeholder' => 'Nombre de cuisine', 'min' => 0]
            ])
            ->add('nbrSalleDeBain', null, [
                'label' => 'Nombre salle de bain', 
                'attr' => ['placeholder' => 'Nombre de salle de bain', 'min' => 0]
            ])
            ->add('extraLogements', null, [
                'label' => 'Extras-Logement',
                'help' => 'Autres avatages disponibles pour votre propriété',
                'attr' => ['class' => 'ms']
            ])
        ;
    }
}