<?php

namespace App\Form\Thing;

use App\Entity\Studio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\PJ\PjStudioType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class StudioType extends AbstractType
{
    public function getParent() {
        return LogementType::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('nbrChambre')
            ->add('estModerne', ChoiceType::class, [
                'label' => 'Est moderne?', 
                'choices' => 
                    array(
                        'Oui' => true,
                        'Non' => false
                    ),
                    'help' => 'Cette chambre est-elle moderne?'
            ]) 
            ->add('piecesJointes', CollectionType::class, [
                'entry_type' => PjStudioType::class,
                'entry_options' => ['label' => false, 'required' => true],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Studio::class,
        ]);
    }
}
