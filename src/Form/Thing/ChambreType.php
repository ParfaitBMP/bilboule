<?php

namespace App\Form\Thing;

use App\Entity\Chambre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Form\PJ\PjChambreType;

class ChambreType extends AbstractType
{
    public function getParent() {
        return LogementType::class;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('nbrSalon')
            ->remove('nbrChambre')
            ->add('estModerne', ChoiceType::class, [
                'label' => 'Est moderne?', 
                'choices' => 
                    array(
                        'Oui' => true,
                        'Non' => false
                    ),
                    'help' => 'Ce studio est-il moderne?'
            ]) 
            ->add('piecesJointes', CollectionType::class, [
                'entry_type' => PjChambreType::class,
                'entry_options' => ['label' => false, 'required' => true],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Chambre::class,
        ]);
    }
}
