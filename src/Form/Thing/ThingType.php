<?php 

namespace App\Form\Thing;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Form\PriceType;
use App\Form\Map\LocalisationType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ThingType extends AbstractType
{
	
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nom', 
                'attr' => ['placeholder' => 'Définir le nom'],
                'help' => 'Donnez un nom à votre propriété afin de facilement l\'identifier. Exemple: "Résidence des princes"'
            ])
            ->add('description', null, [
            	'attr' => [
            		'class' => 'acc-desc', 
            		'placeholder' => 'Décrivez votre propriété ici.'
            	]
            ])
            ->add('operation', ChoiceType::class, 
                array('choices' => 
                    array(
                        'Location' => 'LOCATION',
                        'Vente' => 'VENTE'
                    ),
                    'help' => 'Cette propriété est-elle à vendre ou à louer?'
            )) 
            ->add('localisation', LocalisationType::class)
            ->add('price', PriceType::class, ['label' => false])
        ;
    }
}