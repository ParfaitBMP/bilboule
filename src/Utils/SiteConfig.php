<?php 

namespace App\Utils;

/**
 * 
 */
class SiteConfig
{
	public const SAVE_MSG = 'Informations enregistrées.';

	public const DELETE_MSG = 'Suppression effectuée.';

	public const PUBLISH_MSG = 'Votre annonce a été actualisée. Elle est maintenant en cours de validation. Vous serez notifiés du début de la diffusion.';
}



